[![Build status](https://gitlab.com/fftr/forteso/badges/master/build.svg)](https://gitlab.com/fftr/forteso/commits/master)


Forteso (**F**lexibles **Ort**skunde **e**-Learning **S**ystem, **o**ffline) ist eine offline-fähige Lernanwendung zur Verbesserung der Ortskenntnis.
Das Programm wählt zufällig eine Straße bzw. einen POI aus dem Suchbereich aus, diesen muss der Nutzer auf der Karte finden. Wählt der Nutzer einen Punkt auf der Karte aus, zeigt die Anwendung den gesuchten Straßenverlauf und die Distanz zum gewählten Punkt an.

## Features
+ Austauschbares, flexibles und genaues Kartenmaterial von [Open Street Map](https://www.openstreetmap.org/)
+ Unterstützt selbst eingetragene POIs (Points of Interest) über kartesische Koordinaten oder über Adresse
+ Such-/Abfragebereich beliebig über Straßenfilter einschränkbar

![](images/street_found.png)
> Suchen und finden einer zufällig ausgewählten Straße auf der Karte. Zur Auflösung wird der gesuchte Straßenverlauf hervorgehoben

![](images/poi_found.png)
> Suchen und finden eines Point of Interest, hier eine S-Bahn-Station. Zur Auflösung wird der gesuchte Kartenpunkt angezeigt

![](images/enable_legend.png)
> Beschriftung der Karte kann ein- und ausgeschalten werden

## Verwendung
Forteso ist unter verschiedenen Lizenzen kostenlos verwendbar:
- Fortenso (Client-Anwendung) unter [GPLv3](client/COPYING)
- Fortenso Management & Deployment Tool unter [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 ](https://creativecommons.org/licenses/by-nc-sa/4.0/)


**Download** des Trainingsprogramms [v0.8](https://gitlab.com/fftr/forteso/builds/16124617/artifacts/browse/client/jar/)

**Download** des Programms zur Kartendatenauswahl [v0.8](https://gitlab.com/fftr/forteso/builds/16124618/artifacts/browse/forteso-mdt/jar/)

## Feedback
Einen Fehler melden:
 Eine Email mit Fehlerbeschreibung an incoming+fftr/forteso [at] gitlab.com