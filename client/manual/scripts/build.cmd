@echo off

set ZIM=C:\Program Files (x86)\Zim Desktop Wiki\zim.exe
if not exist "%ZIM%" set ZIM=C:\Program Files\Zim Desktop Wiki\zim.exe
if not exist "%ZIM%" set ZIM=zim.exe

cd /d "%~dp0"
cd ..

mkdir ..\rsc\help\manual

"%ZIM%" --export ^
  --format=html --template="templates\html\Forteso Manual HTML.html" ^
  --output=..\rsc\help\manual --index-page=index ^
  --root-url=..\rsc --overwrite ^
  .\src