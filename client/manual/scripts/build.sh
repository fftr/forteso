#!/bin/bash
 cd "`dirname "$0"`"
 cd ..

 mkdir -p ../rsc/help/manual
 rm -rf ../rsc/help/manual/*
 zim --export \
   --format=html --template="templates/html/Forteso Manual HTML.html" \
   --output=../rsc/help/manual --index-page=index \
   --root-url=../rsc --overwrite \
   ./src