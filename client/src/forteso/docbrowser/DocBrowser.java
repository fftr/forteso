/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.docbrowser;

import java.net.URL;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLAnchorElement;

import edu.stanford.ejalbert.BrowserLauncher;
import edu.stanford.ejalbert.exception.BrowserLaunchingInitializingException;
import edu.stanford.ejalbert.exception.UnsupportedOperatingSystemException;
import forteso.utils.DialogUtils;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;

public class DocBrowser {

    private WebEngine engine;

    public DocBrowser() {
    }

    public void show(URL url, String title) {
	Dialog<Void> dialog = new Dialog<>();
	dialog.setTitle(title);
	VBox box = new VBox();
	Hyperlink backLink = new Hyperlink("Zurück");
	WebView view = new WebView();
	engine = view.getEngine();
	engine.getHistory().setMaxSize(Integer.MAX_VALUE);
	engine.setJavaScriptEnabled(true);
	engine.getLoadWorker().stateProperty().addListener(this::onPageLoaded);
	box.getChildren().add(backLink);
	box.getChildren().add(view);
	backLink.setOnAction(this::back);

	engine.load(url.toString());
	dialog.getDialogPane().setContent(box);
	dialog.getDialogPane().getButtonTypes()
		.add(new ButtonType("Schließen", ButtonData.OK_DONE));
	dialog.show();
    }

    private void back(ActionEvent e) {
	WebHistory hist = engine.getHistory();
	if (hist.getCurrentIndex() > 0) {
	    hist.go(-1);// one page back
	}
    }

    private void onPageLoaded(ObservableValue<? extends Worker.State> obs,
	    Worker.State old, Worker.State newS) {
	if (newS.equals(Worker.State.SUCCEEDED)) {
	    NodeList nodeList = engine.getDocument().getElementsByTagName("a");
	    for (int i = 0; i < nodeList.getLength(); i++) {
		Node node = nodeList.item(i);
		EventTarget eventTarget = (EventTarget) node;
		eventTarget.addEventListener("click", new EventListener() {
		    @Override
		    public void handleEvent(Event evt) {
			EventTarget target = evt.getCurrentTarget();
			HTMLAnchorElement anchorElement = (HTMLAnchorElement) target;
			String href = anchorElement.getHref();
			if (href.startsWith("http")
				|| href.startsWith("www.")) {
			    // open external link in browser
			    evt.preventDefault();
			    openDesktopBrowser(href);
			}

		    }
		}, false);
	    }
	}
    }

    private void openDesktopBrowser(String url) {
	try {
	    BrowserLauncher launcher = new BrowserLauncher();
	    launcher.openURLinBrowser(url);
	} catch (BrowserLaunchingInitializingException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Öffnen der Standardanwendung Web-Browser", e);
	} catch (UnsupportedOperatingSystemException e) {
	    DialogUtils.showInfoDialog("Kein Browser gefunden!",
		    "Um diesen Link (" + url
			    + ") zu öffnen muss ein Internetbrowser installiert und als "
			    + "Standardanwendung eingerichtet sein. Eventuell wird dieses Betriebssystem nicht unterstützt");
	}
    }

}
