/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Level;

import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.layer.Layer;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.overlay.Polyline;

import forteso.main.FortesoApplication;
import forteso.main.TerminationListener;
import forteso.visualization.Markertype;
import forteso.visualization.OverlayManager;

public class ClientOverlayManager
	implements TerminationListener, OverlayManager {

    private Layers layers;
    private final Map<String, List<Layer>> overlayElements;

    public ClientOverlayManager(Layers l) {
	FortesoApplication.terminationCallback.add(this);
	layers = l;
	overlayElements = new TreeMap<>();
    }

    @Override
    public BMPMarker addMarker(String group, LatLong latlon, Markertype type) {
	try {
	    BMPMarker m = new BMPMarker(latlon, type);
	    add(m, group);
	    return m;
	} catch (IOException e) {
	    FortesoApplication.LOG.log(Level.SEVERE,
		    "Cannot create BMPMarker at " + latlon + " of type "
			    + type.toString(),
		    e);
	    throw new RuntimeException(e);
	}
    }

    @Override
    public Polyline addLine(String group, List<LatLong> points, Paint p) {
	Polyline line = new Polyline(p, AwtGraphicFactory.INSTANCE, true);
	List<LatLong> latlongs = line.getLatLongs();
	for (int i = 0; i < Math.min(points.size(), Integer.MAX_VALUE); ++i) {
	    latlongs.add(points.get(i));
	}
	add(line, group);
	return line;
    }

    private void add(Layer layer, String group) {
	List<Layer> list = overlayElements.computeIfAbsent(group,
		key -> new ArrayList<Layer>());
	list.add(layer);
	layers.add(layer);
    }

    @Override
    public void markDistance(String group, LatLong from, LatLong to) {
	List<LatLong> distancePoints = new ArrayList<>();
	distancePoints.add(from);
	distancePoints.add(to);
	addLine(group, distancePoints, ClientOverlayManager.DISTANCE_PAINT);
    }

    @Override
    public void removeAll() {
	overlayElements.values().forEach(list -> list.forEach(layers::remove));
	overlayElements.clear();
    }

    @Override
    public void remove(String group) {
	Optional.ofNullable(overlayElements.get(group)).ifPresent(list -> {
	    list.forEach(layers::remove);
	    list.clear();
	});
    }

    @Override
    public void onExit(int status) {
	removeAll();
    }

}
