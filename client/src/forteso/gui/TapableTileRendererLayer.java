/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.gui;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.mapsforge.core.graphics.GraphicFactory;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.map.datastore.MapDataStore;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.layer.renderer.TileRendererLayer;
import org.mapsforge.map.model.MapViewPosition;

import javafx.application.Platform;

public class TapableTileRendererLayer extends TileRendererLayer {

    private List<OnTapListener> listeners;

    public TapableTileRendererLayer(TileCache tileCache,
	    MapDataStore mapDataStore, MapViewPosition mapViewPosition,
	    boolean isTransparent, boolean renderLabels, boolean cacheLabels,
	    GraphicFactory graphicFactory) {
	super(tileCache, mapDataStore, mapViewPosition, isTransparent,
		renderLabels, cacheLabels, graphicFactory);
	listeners = new LinkedList<>();
    }

    public void addOnTapListener(OnTapListener l) {
	listeners.add(Objects.requireNonNull(l));
    }

    @Override
    public boolean onTap(LatLong tapLatLong, Point layerXY, Point tapXY) {
	Platform.runLater(() -> {
	    listeners.stream().forEach(
		    (listener) -> listener.onTap(tapLatLong, layerXY, tapXY));
	});
	return super.onTap(tapLatLong, layerXY, tapXY);
    }

}
