/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.gui.controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.reader.MapFile;

import forteso.gui.ClientOverlayManager;
import forteso.trainer.FindResult;
import forteso.trainer.ModelInterface;
import forteso.utils.DialogUtils;
import forteso.visualization.Markertype;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class GuiController implements Initializable {
    private static final double MIN_SIZE = 11;
    private static final double DEFAULT_SIZE = 15;
    private static final double MAX_SIZE = 50;

    private static final String CSS_ROOT_CLASS = ".root";
    private static final String CSS_FONT_SIZE = "-fx-font-size: %fpx";

    @FXML
    private Node rootContainer;

    @FXML
    private Label searchStreetLabel;
    @FXML
    private Label distanceLabel;
    @FXML
    private SwingNode mapViewContainer;
    @FXML
    private Button nextExerciseButton;
    @FXML
    private Slider textSizeSlider;
    @FXML
    private Hyperlink copyrightOSM;

    private ClientOverlayManager overlayManager;
    private MapViewController mapViewControl;
    private LabelController labelControl;
    private ExerciseController exerciseControl;

    private ModelInterface model;

    public GuiController(MapFile mapFile, ModelInterface model) {
	this.model = model;
	mapViewControl = new MapViewController(mapFile);
	exerciseControl = new ExerciseController();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	overlayManager = new ClientOverlayManager(
		mapViewControl.getMapLayers());
	// insert home marker, if any
	model.getHome()
		.ifPresent(latlon -> overlayManager.addMarker(
			ClientOverlayManager.GROUP_HOME, latlon,
			Markertype.HOME));

	labelControl = new LabelController(searchStreetLabel.getText());

	JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	panel.add(mapViewControl.getMapView());
	mapViewContainer.setContent(panel);

	nextExerciseButton
		.setOnAction(event -> exerciseControl.startExercise());
	mapViewControl.addOnTapListener(
		(latlong, layerXY, tapXY) -> exerciseControl.onTap(latlong));

	// SLIDER FOR CHOSING TEXT SIZE
	textSizeSlider.setMin(MIN_SIZE);
	textSizeSlider.setMax(MAX_SIZE);
	textSizeSlider.setBlockIncrement(1.0d);
	textSizeSlider.setValue(DEFAULT_SIZE);
	textSizeSlider.valueProperty()
		.addListener((ov, old, newV) -> applyStyle(CSS_ROOT_CLASS,
			String.format(CSS_FONT_SIZE, newV.doubleValue())));

	// COPYRIGHT HYPERLINK
	copyrightOSM.setOnAction(event -> MenuController.showAboutDialog());
	// disable visited effect
	copyrightOSM.visitedProperty().addListener((obsV, old, newV) -> {
	    if (newV) {
		copyrightOSM.setVisited(false);
	    }
	});

	// start exercise
	exerciseControl.startExercise();
    }

    public MapViewController getMapViewControl() {
	return mapViewControl;
    }

    private void applyStyle(String viewClass, String style) {
	for (Node node : rootContainer.lookupAll(viewClass)) {
	    node.setStyle(style);
	}
    }

    ModelInterface getModel() {
	return model;
    }

    private class LabelController {
	private String streetPrefix;
	private static final String distancePrefix = "Distanz: ";
	private static final String distanceSuffix = "m  ";
	private static final String textNoSearchData = "<leerer Suchraum>";

	public LabelController(String streetPrefix) {
	    this.streetPrefix = streetPrefix;
	}

	public void showStreetName(String streetname) {
	    searchStreetLabel.setText(streetPrefix + streetname);
	}

	public void showNoSearchData() {
	    searchStreetLabel.setText(textNoSearchData);
	}

	public void displayDistance(double distance) {
	    distanceLabel.setText(distancePrefix + distance + distanceSuffix);
	}
    }

    private class ExerciseController {
	private boolean waitingForTap;
	private String streetname;

	public void onTap(LatLong latlong) {
	    if (waitingForTap) {
		// evaluate
		FindResult result = model.evaluateTap(streetname, latlong);
		overlayManager.addMarker(
			ClientOverlayManager.GROUP_HIGHLIGHT_SEARCH_RES,
			result.getTap(), Markertype.GREEN);
		result.getEntity().visualize(overlayManager);
		overlayManager.markDistance(
			ClientOverlayManager.GROUP_HIGHLIGHT_SEARCH_RES,
			result.getTap(), result.getNearestNode());
		double distance = Math.round(LatLongUtils.vincentyDistance(
			result.getTap(), result.getNearestNode()));
		labelControl.displayDistance(distance);
		waitingForTap = false;
	    }
	}

	public void startExercise() {
	    Optional<String> nameOpt = model.getRandomEntityKey();
	    if (nameOpt.isPresent()) {
		streetname = nameOpt.get();
		labelControl.showStreetName(streetname);
		waitingForTap = true;
	    } else {
		DialogUtils.showInfoDialog("Suchraum leer",
			"Keine Objekte zum Fragen gefunden");
		labelControl.showNoSearchData();
		waitingForTap = false;
	    }
	    distanceLabel.setText("");
	    overlayManager
		    .remove(ClientOverlayManager.GROUP_HIGHLIGHT_SEARCH_RES);

	}

    }
}
