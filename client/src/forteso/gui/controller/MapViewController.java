/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.gui.controller;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.util.UUID;

import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.MapPosition;
import org.mapsforge.core.util.LatLongUtils;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.awt.util.AwtUtil;
import org.mapsforge.map.awt.view.MapView;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.cache.TileCache;
import org.mapsforge.map.reader.MapFile;
import org.mapsforge.map.rendertheme.InternalRenderTheme;

import forteso.gui.OnTapListener;
import forteso.gui.TapableTileRendererLayer;
import forteso.main.FortesoApplication;
import forteso.main.TerminationListener;

class MapViewController implements TerminationListener {

    private static final int TILE_SIZE = 512;
    private static final int TILE_CACHE_CAPACITY = 1024;

    private MapView mapView;
    private TapableTileRendererLayer labeledMapLayer;
    private TapableTileRendererLayer unlabeledMapLayer;
    private BoundingBox initialBoundingBox;

    private final byte MAX_ZOOMLEVEL = (byte) 20;

    MapViewController(MapFile mapFile) {
	FortesoApplication.terminationCallback.add(this);
	buildMapView(mapFile);
    }

    public void centerView() {
	byte zoomLevel = LatLongUtils.zoomForBounds(
		mapView.getModel().mapViewDimension.getDimension(),
		initialBoundingBox,
		mapView.getModel().displayModel.getTileSize());
	mapView.getModel().mapViewPosition.setMapPosition(new MapPosition(
		initialBoundingBox.getCenterPoint(), zoomLevel));
	mapView.setZoomLevelMin(zoomLevel); // rauszoomen bis zoomlevel erlaubt
	mapView.setZoomLevelMax(MAX_ZOOMLEVEL); // reinzoomen bis MAX_ZOOMLEVEL
						// erlaubt

    }

    private void buildMapView(MapFile layer) {
	mapView = new MapView();
	mapView.getMapScaleBar().setVisible(true);
	mapView.setCenter(mapView.getBoundingBox().getCenterPoint());
	mapView.addComponentListener(new ComponentListenerImpl());

	// get the mousewheellistener in front of the mapView mouse wheel
	// listener
	MouseWheelListener[] others = mapView.getMouseWheelListeners();
	for (MouseWheelListener lis : others) {
	    mapView.removeMouseWheelListener(lis);
	}
	mapView.addMouseWheelListener(event -> {
	    if (event.getWheelRotation() < 0) {
		LatLong cursor = mapView.getMapViewProjection()
			.fromPixels(event.getX(), event.getY());
		mapView.getModel().mapViewPosition.setPivot(cursor);
		mapView.getModel().mapViewPosition.setCenter(cursor);
	    }
	});
	for (MouseWheelListener lis : others) {
	    mapView.addMouseWheelListener(lis);
	}

	Layers layers = mapView.getLayerManager().getLayers();

	TileCache tileCacheLabeled = AwtUtil.createTileCache(
		mapView.getModel().displayModel.getTileSize(),
		mapView.getModel().frameBufferModel.getOverdrawFactor(),
		TILE_CACHE_CAPACITY,
		new File(System.getProperty("java.io.tmpdir"),
			UUID.randomUUID().toString()));

	TileCache tileCacheUnlabeled = AwtUtil.createTileCache(
		mapView.getModel().displayModel.getTileSize(),
		mapView.getModel().frameBufferModel.getOverdrawFactor(),
		TILE_CACHE_CAPACITY,
		new File(System.getProperty("java.io.tmpdir"),
			UUID.randomUUID().toString()));

	mapView.getModel().displayModel.setFixedTileSize(TILE_SIZE);

	labeledMapLayer = new TapableTileRendererLayer(tileCacheLabeled, layer,
		mapView.getModel().mapViewPosition, false, /*-->*/ true, false,
		AwtGraphicFactory.INSTANCE);
	labeledMapLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);
	unlabeledMapLayer = new TapableTileRendererLayer(tileCacheUnlabeled,
		layer, mapView.getModel().mapViewPosition, false, false, false,
		AwtGraphicFactory.INSTANCE);
	unlabeledMapLayer.setXmlRenderTheme(InternalRenderTheme.OSMARENDER);
	layers.add(unlabeledMapLayer);
	layers.add(labeledMapLayer);
	labeledMapLayer.setVisible(false, true);
	initialBoundingBox = layer.boundingBox();
    }

    public void showLabels(boolean show) {
	if (show) {
	    unlabeledMapLayer.setVisible(false, true);
	    labeledMapLayer.setVisible(true, true);
	} else {
	    labeledMapLayer.setVisible(false, true);
	    unlabeledMapLayer.setVisible(true, true);
	}
    }

    @Override
    public void onExit(int status) {
	if (mapView != null) {
	    mapView.destroyAll();
	}

	AwtGraphicFactory.clearResourceMemoryCache();
    }

    public java.awt.Component getMapView() {
	return mapView;
    }

    public Layers getMapLayers() {
	return mapView.getLayerManager().getLayers();
    }

    public void addOnTapListener(OnTapListener l) {
	// labeledMapLayer.addOnTapListener(l); Unnecessary because both layers
	// are added
	unlabeledMapLayer.addOnTapListener(l);
    }

    private class ComponentListenerImpl implements ComponentListener {

	@Override
	public void componentResized(ComponentEvent e) {
	    centerView();
	}

	@Override
	public void componentMoved(ComponentEvent e) {

	}

	@Override
	public void componentShown(ComponentEvent e) {

	}

	@Override
	public void componentHidden(ComponentEvent e) {

	}
    }
}
