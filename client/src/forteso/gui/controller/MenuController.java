/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.gui.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Objects;
import java.util.ResourceBundle;

import forteso.docbrowser.DocBrowser;
import forteso.io.MapDataIO;
import forteso.main.FortesoApplication;
import forteso.trainer.ModelInterface;
import forteso.utils.DialogUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;

public class MenuController implements Initializable {
    private static final URL aboutFile = Objects.requireNonNull(
	    MenuController.class.getResource("/help/about.html"),
	    () -> "About file not found");
    private static final URL helpFile = Objects.requireNonNull(
	    MenuController.class.getResource("/help/manual/index.html"),
	    () -> "Help file not found");
    public static final URL quickstartFile = Objects.requireNonNull(
	    MenuController.class.getResource("/help/manual/Schnellstart.html"),
	    () -> "Quickstart file not found");

    @FXML
    private MenuItem closeApplicationItem;
    @FXML
    private MenuItem resetViewItem;
    @FXML
    private CheckMenuItem showLabelsItem;

    @FXML
    private MenuItem generateStreetnameFileItem;

    @FXML
    private CheckMenuItem enableStreetnamesItem;
    @FXML
    private CheckMenuItem enablePOIsItem;
    @FXML
    private MenuItem aboutItem;
    @FXML
    private MenuItem helpItem;

    public MenuController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	closeApplicationItem.setOnAction(event -> FortesoApplication.quit(0));
	resetViewItem.setOnAction(event -> FortesoApplication.MAIN_CONTROLLER
		.ifPresent(root -> root.getMapViewControl().centerView()));
	showLabelsItem.setSelected(false);
	showLabelsItem.setOnAction(event -> {
	    FortesoApplication.MAIN_CONTROLLER
		    .ifPresent(root -> root.getMapViewControl()
			    .showLabels(showLabelsItem.isSelected()));
	});

	generateStreetnameFileItem
		.setOnAction(event -> showExportStreetNames());

	boolean useStreets = false;
	boolean usePOIs = false;
	if (FortesoApplication.MAIN_CONTROLLER.isPresent()) {
	    ModelInterface model = FortesoApplication.MAIN_CONTROLLER.get()
		    .getModel();
	    useStreets = model.useStreets();
	    usePOIs = model.usePOIs();
	}

	enableStreetnamesItem.setSelected(useStreets);
	enablePOIsItem.setSelected(usePOIs);

	enableStreetnamesItem
		.setOnAction(event -> FortesoApplication.MAIN_CONTROLLER
			.ifPresent(root -> root.getModel().useStreets(
				enableStreetnamesItem.isSelected())));
	enablePOIsItem.setOnAction(event -> FortesoApplication.MAIN_CONTROLLER
		.ifPresent(root -> root.getModel()
			.usePOIs(enablePOIsItem.isSelected())));
	aboutItem.setOnAction(event -> showAboutDialog());
	helpItem.setOnAction(
		event -> new DocBrowser().show(helpFile, "Anleitung"));
    }

    private void showExportStreetNames() {
	FileChooser fc = new FileChooser();
	fc.setTitle("Wähle Speicherort");
	File dest = fc.showSaveDialog(
		generateStreetnameFileItem.getParentPopup().getOwnerWindow());
	if (dest != null) {
	    try {
		Collection<String> streets = FortesoApplication.INSTANCE
			.getTrainer().getArea().getStreetNames();
		MapDataIO.writeStreetnameList(dest,
			FortesoApplication.CONFIG.getCharset(), streets);
	    } catch (IOException e) {
		DialogUtils.showBlockingErrorDialog(
			"Fehler beim Schreiben der Datei", e);
	    }
	}
    }

    public static void showAboutDialog() {
	new DocBrowser().show(aboutFile, "Über dieses Programm");
	// try (InputStream is = aboutFile.openStream()) {
	//
	// DialogUtils.showDynamicInfoDialog("Über dieses Programm", is,
	// MapDataIO.DEFAULT_CHARSET);
	// } catch (IOException e) {
	// DialogUtils.showBlockingErrorDialog("Fehler beim Anzeigen", e);
	// }
    }

}
