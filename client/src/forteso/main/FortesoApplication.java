/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.main;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.parsers.ParserConfigurationException;

import org.mapsforge.map.reader.MapFile;

import forteso.docbrowser.DocBrowser;
import forteso.gui.controller.GuiController;
import forteso.gui.controller.MenuController;
import forteso.io.MapDataCorruptedException;
import forteso.io.MapDataIO;
import forteso.trainer.Trainer;
import forteso.trainer.TrainingArea;
import forteso.utils.AddOnlyCollection;
import forteso.utils.DialogUtils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FortesoApplication extends Application {
    private static final String layout_application = "/resources/layout_application.fxml";
    private static final String TITLE = "Forteso";
    private static final String VERSION = "0.8";
    private static final String LOGFILE = "log_";
    private static final String LOGFILE_NAMING_PATTERN = "yyyy-M-dd_H-mm-ss";

    public static final AddOnlyCollection<TerminationListener> terminationCallback = AddOnlyCollection
	    .wrapAsAddOnly(new LinkedList<>());
    private static int errorCode = 0;

    // MAIN WINDOW CONTROLLER (CONTROLLER ROOT)
    public static Optional<GuiController> MAIN_CONTROLLER = Optional.empty();

    // PROPERTIES
    public static FortesoProperties CONFIG;

    // LOGGER
    public static Logger LOG;

    // INSTANCE
    public static FortesoApplication INSTANCE;

    private GuiController gui;
    private Trainer trainer;

    public FortesoApplication() {
	INSTANCE = this;
    }

    private void initGui() {

	File binaryMapFile = new File(CONFIG.getBinaryMapPath().getFile());
	if (CONFIG.getBinaryMapPath().getProtocol().equals("jar")) {
	    // extract the binary file first, as random access inside a
	    // compressed jar is not possible
	    try (InputStream is = CONFIG.getBinaryMapPath().openStream()) {
		binaryMapFile = File.createTempFile("forteso_binary_map",
			"map");
		binaryMapFile.mkdirs();
		Files.copy(is, binaryMapFile.toPath(),
			StandardCopyOption.REPLACE_EXISTING);
	    } catch (IOException e) {
		throw new RuntimeException(
			"Cannot extract binarized map file from "
				+ CONFIG.getBinaryMapPath() + " to "
				+ binaryMapFile.getAbsolutePath(),
			e);
	    }
	}

	gui = new GuiController(new MapFile(binaryMapFile), trainer);
	MAIN_CONTROLLER = Optional.ofNullable(gui);
    }

    public GuiController getGui() {
	return gui;
    }

    public Trainer getTrainer() {
	return trainer;
    }

    private void loadTrainingArea() {
	URL resource = CONFIG.getLocationMapPath();
	try {
	    TrainingArea area = MapDataIO.parse(resource);
	    trainer = new Trainer(area);
	} catch (ParserConfigurationException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "When parsing " + resource.toString(), e);
	    throw new RuntimeException(e);
	} catch (MapDataCorruptedException e) {
	    DialogUtils
		    .showBlockingErrorDialog("Map data seems to be corrupted ("
			    + resource.toString() + ")", e);
	    throw new RuntimeException(e);
	} catch (IOException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "I/O Error when parsing " + resource.toString(), e);
	    throw new RuntimeException(e);
	}

    }

    private void forceQuit(int status) {
	LOG.log(Level.INFO,
		"Exiting application: Calling termination callbacks");
	for (TerminationListener l : terminationCallback) {
	    try {
		l.onExit(status);
	    } catch (Throwable t) {
		LOG.log(Level.SEVERE, "Termination callback threw exception",
			t);
	    }
	}
	LOG.log(Level.INFO, "Exiting application");
	System.exit(status);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
	try {
	    CONFIG = new FortesoProperties();
	    loadTrainingArea();
	    initGui();

	    URL res = getClass().getResource(layout_application);
	    FXMLLoader loader = new FXMLLoader(res);
	    loader.setController(gui);
	    Parent root = loader.load();
	    primaryStage.setTitle(TITLE + " " + VERSION);
	    primaryStage.setScene(new Scene(root));

	    // show welcome dialog with manual
	    new DocBrowser().show(MenuController.quickstartFile, "Willkommen");

	    primaryStage.show();
	    primaryStage.toBack();
	} catch (Throwable t) {
	    LOG.log(Level.SEVERE, "Throwable in application start", t);
	    throw t;
	}
    }

    @Override
    public void stop() {
	forceQuit(errorCode);
    }

    public static void quit(int code) {
	errorCode = code;
	Platform.exit();
    }

    public static void main(String[] args) {

	// SETUP Logger
	setupLogger(args);
	launch(FortesoApplication.class, args);
    }

    private static void setupLogger(String[] args) {
	LOG = Logger.getGlobal();
	try {
	    FileHandler fileHandler = new FileHandler(LOGFILE
		    + DateTimeFormatter.ofPattern(LOGFILE_NAMING_PATTERN)
			    .format(LocalDateTime.now())
		    + ".log");
	    fileHandler.setFormatter(new SimpleFormatter());
	    LOG.addHandler(fileHandler);
	} catch (SecurityException | IOException e) {
	    LOG.log(Level.WARNING, "Cannot create logfile", e);
	}

	Level loggerLevel = Level.WARNING;// default is warning or higher
	if (args.length > 0) {
	    if (Arrays.stream(args).anyMatch("-v"::equalsIgnoreCase)) {
		loggerLevel = Level.INFO;
	    }
	}
	LOG.setLevel(loggerLevel);
    }

}
