/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;

import forteso.io.parserextensions.ParserExtensionBundle;
import forteso.io.parserextensions.ParserModule;
import forteso.trainer.POI;
import forteso.trainer.Street;
import forteso.trainer.TrainingArea;

public class MapDataIO {
    static final Logger IO_LOG = Logger.getLogger(MapDataIO.class.getName());
    public static final Charset DEFAULT_CHARSET = Objects.requireNonNull(
	    Charset.forName("utf8"), "Didn't find UTF-8 charset");

    private MapDataIO() {
	throw new IllegalStateException("not intended to instantiate");
    }

    public static TrainingArea parse(URL resource)
	    throws ParserConfigurationException, MapDataCorruptedException,
	    IOException {

	MapDataIO.IO_LOG.info(() -> "Parsing resource " + resource.toString());

	ParserExtensionBundle bundle = ParserModule.createConfiguration(
		ParserModule.BOUNDING_BOX, ParserModule.HOME_MARKER,
		ParserModule.POIS, ParserModule.STREETS);
	parse(resource, bundle);
	// construct Training Area
	Optional<BoundingBox> optBox = bundle
		.getParsedData(ParserModule.BOUNDING_BOX);
	Collection<Street> streets = bundle.getParsedData(ParserModule.STREETS);
	Collection<POI> pois = bundle.getParsedData(ParserModule.POIS);
	BoundingBox box = optBox.orElseThrow(
		() -> new MapDataCorruptedException("No BoundingBox found"));
	Optional<LatLong> homeMarker = bundle
		.getParsedData(ParserModule.HOME_MARKER);

	TrainingArea area = new TrainingArea(box, homeMarker);
	streets.forEach(area::addStreet);
	pois.forEach(area::addPOI);
	return area;
    }

    public static void parse(URL resource, ParserExtensionBundle bundle)
	    throws IOException, MapDataCorruptedException {
	Parser parser = new Parser();
	parser.parse(resource, bundle);
    }

    public static OsmDataWriter getWriter() {
	return new Writer();
    }

    public static void writeOptimizedMap(File destination, TrainingArea mapArea,
	    Charset charset) throws IOException {

	OsmDataWriter w = new Writer();
	OsmDocumentBuilder builder = w.startDocument();
	builder.addAllPOIs(mapArea.getPOIs());
	builder.addAllStreets(mapArea.getStreets());
	builder.setBoundingBox(mapArea.getBoundingBox(), true);
	try (OutputStream fos = new FileOutputStream(destination)) {
	    // try-with-resources
	    w.writeDocument(builder, fos, charset);
	} catch (XMLStreamException | FactoryConfigurationError e) {
	    throw new IOException(e);
	}
    }

    public static Whitelist parseWhitelist(URL resource, Charset charset)
	    throws IOException {
	try (InputStream is = resource.openStream()) {
	    // try-with-resources
	    return readWhitelist(is, charset);
	}
    }

    public static void writeStreetnameList(File destination, Charset charset,
	    Collection<String> streets)
	    throws FileNotFoundException, IOException {
	OsmDataWriter w = new Writer();

	try (OutputStream os = new FileOutputStream(destination)) {
	    // try-with-resources
	    w.writeStreetnameList(streets, os, charset);
	}

    }

    private static Whitelist readWhitelist(InputStream is, Charset c)
	    throws IOException {
	BufferedReader reader = new BufferedReader(
		new InputStreamReader(is, c));
	String line = null;
	Set<String> names = new TreeSet<>();
	while ((line = reader.readLine()) != null) {
	    String name = line.trim();
	    if (!names.add(name)) {
		IO_LOG.log(Level.WARNING,
			"Duplicate entry '" + name + "' in Whitelist");
	    }
	}
	return new Whitelist(names);
    }

}
