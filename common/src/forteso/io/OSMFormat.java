/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

public final class OSMFormat {
    public static final String ROOT = "osm";
    public static final String NODE = "node";
    public static final String KEY_VALUE_NODE = "tag";
    public static final String WAY = "way";
    public static final String WAY_NODE = "nd";
    public static final String BOUNDING_BOX = "bounds";
    public static final String POI = "poi";
    public static final String ATTR_POI_LABEL = "label";
    public static final String ATTR_ID = "id";
    public static final String ATTR_LAT = "lat";
    public static final String ATTR_LONG = "lon";
    public static final String ATTR_KEY = "k";
    public static final String ATTR_VALUE = "v";
    public static final String ATTR_REF = "ref";
    public static final String ATTR_MINLAT = "minlat";
    public static final String ATTR_MINLON = "minlon";
    public static final String ATTR_MAXLAT = "maxlat";
    public static final String ATTR_MAXLON = "maxlon";

    public static final String TAG_STREETNAME = "name";
    public static final String TAG_STREET_TYPE = "highway";
    public static final String[] VALID_STREET_TYPES = { "residential",
	    "primary", "secondary", "motorway", "trunk", "tertiary",
	    "unclassified", "service" };

    public static final String TAG_HOUSENUMBER = "addr:housenumber";
    public static final String TAG_STREETADDRESS = "addr:street";

    // HOME MARKER
    public static final String HOME = "home";

    private OSMFormat() {
	throw new IllegalStateException("not intended to initialize this");
    }

}
