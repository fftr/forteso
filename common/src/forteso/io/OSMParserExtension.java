/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

import java.util.Objects;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public interface OSMParserExtension {

    void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException;

    void handleEndElement(String uri, String localName, String qName)
	    throws SAXException;

    void parsingFinished() throws SAXException;

    default void setDependency(OSMParserExtension dep)
	    throws ParserConfigurationException {
	throw new ParserConfigurationException("Parser " + getClass().getName()
		+ " declared a dependency but did not overwrite this method");
    }

    default void setOptionalDependency(OSMParserExtension optDep)
	    throws ParserConfigurationException {
	throw new ParserConfigurationException("Parser " + getClass().getName()
		+ " declared an optional dependency but did not overwrite this method");
    }

    public static long parseLong(Attributes a, String key) throws SAXException {
	String longString = a.getValue(key);
	if (longString == null) {
	    throw new SAXException("Expected value for key '" + key
		    + "' in attributes " + toString(a));
	}
	try {
	    return Long.parseLong(longString);
	} catch (NumberFormatException e) {
	    throw new SAXException("Value of key '" + key
		    + "' is expected to be a long integer (" + toString(a)
		    + ")", e);
	}
    }

    public static double parseDouble(Attributes a, String key)
	    throws SAXException {
	String doubleString = a.getValue(key);
	if (doubleString == null) {
	    throw new SAXException("Expected value for key '" + key
		    + "' in attributes " + toString(a));
	}
	try {
	    return Double.parseDouble(doubleString);
	} catch (NumberFormatException e) {
	    throw new SAXException("Value of key '" + key
		    + "' is expected to be a double (" + toString(a) + ")", e);
	}
    }

    public static String parseString(Attributes a, String key)
	    throws SAXException {
	String value = a.getValue(key);
	if (value != null) {
	    return value;
	}
	throw new SAXException("Expected value for key '" + key
		+ "' in attributes " + toString(a));
    }

    @SuppressWarnings("unchecked")
    public static <U extends OSMParserExtension, V extends OSMParserExtension> U checkedCast(
	    V v) throws ParserConfigurationException {
	try {
	    return (U) Objects.requireNonNull(v);
	} catch (ClassCastException e) {
	    throw new ParserConfigurationException(
		    "Invalid parser type. Tried to cast "
			    + v.getClass().getName());
	}
    }

    public static String toString(Attributes a) {
	String[] attributes = new String[a.getLength()];
	for (int i = 0; i < attributes.length; i++) {
	    attributes[i] = a.getQName(i) + "=" + a.getValue(i);
	}
	return String.join(", ", attributes);
    }
}
