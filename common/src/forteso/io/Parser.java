/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import forteso.io.parserextensions.ParserExtensionBundle;
import forteso.utils.CollectionUtils;

class Parser extends DefaultHandler implements OsmDataParser {

    private List<OSMParserExtension> extensions;

    Parser() {
	extensions = new LinkedList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName,
	    Attributes attributes) throws SAXException {
	for (OSMParserExtension ext : extensions) {
	    try {
		ext.handleStartElementstartElement(uri, localName, qName,
			attributes);
	    } catch (SAXException e) {
		MapDataIO.IO_LOG.log(Level.WARNING, "", e);
		throw new SAXException("In start element with Parser " + ext
			+ ": qName=" + qName + ", "
			+ OSMParserExtension.toString(attributes));
	    }
	}
    }

    @Override
    public void endElement(String uri, String localName, String qName)
	    throws SAXException {
	for (OSMParserExtension ext : extensions) {
	    try {
		ext.handleEndElement(uri, localName, qName);
	    } catch (SAXException e) {
		MapDataIO.IO_LOG.log(Level.WARNING, "", e);
		throw new SAXException("In end element with Parser " + ext
			+ ": qName=" + qName);
	    }
	}
    }

    @Override
    public void parse(URL resource, ParserExtensionBundle bundle)
	    throws IOException, MapDataCorruptedException {
	extensions.addAll(bundle.getExtensions());
	SAXParserFactory factory = SAXParserFactory.newInstance();
	try {
	    SAXParser saxParser = factory.newSAXParser();
	    InputStream is = resource.openStream();
	    MapDataIO.IO_LOG.info(() -> "Started parsing");
	    saxParser.parse(is, this);

	    CollectionUtils.forEachChecked(extensions,
		    OSMParserExtension::parsingFinished);
	    MapDataIO.IO_LOG.info(() -> "Finished parsing");
	} catch (SAXException e) {
	    throw new MapDataCorruptedException(e);
	} catch (ParserConfigurationException e) {
	    throw new IOException(e);
	}
    }

}
