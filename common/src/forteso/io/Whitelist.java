/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

import java.util.Collections;
import java.util.Set;

public class Whitelist {
    public static final Whitelist ALLOW_ALL_LIST = new Whitelist(
	    Collections.emptySet(), true);

    private boolean allowAll;
    private Set<String> wh;

    private Whitelist(Set<String> s, boolean allowAll) {
	wh = s;
	this.allowAll = allowAll;
    }

    public Whitelist(Set<String> s) {
	this(s, false);
    }

    public boolean isWhitelisted(String s) {
	return allowAll || wh.contains(s);
    }

    public Set<String> listedValues() {
	return Collections.unmodifiableSet(wh);
    }
}