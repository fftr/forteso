/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;

import forteso.trainer.POI;
import forteso.trainer.Street;

class Writer implements OsmDataWriter {

    Writer() {
	// package constructor
    }

    @Override
    public void writeStreetnameList(Collection<String> streets, OutputStream os,
	    Charset c) {
	Objects.requireNonNull(streets);
	Objects.requireNonNull(c);
	PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, c), true);
	streets.stream().forEach(pw::println);
	pw.flush();
    }

    @Override
    public OsmDocumentBuilder startDocument() {
	return new DocBuilder();
    }

    @Override
    public void writeDocument(OsmDocumentBuilder builder, OutputStream os,
	    Charset c) throws XMLStreamException, FactoryConfigurationError {
	XMLStreamWriter xml = XMLOutputFactory.newFactory()
		.createXMLStreamWriter(new OutputStreamWriter(os, c));
	xml.writeStartDocument(c.name(), "1.0");
	builder.writeDocument(xml);
	xml.writeEndDocument();
	xml.flush();
    }

    private static class DocBuilder implements OsmDocumentBuilder {
	private static final Comparator<Street.Node> NODE_ID_COMP = (n1,
		n2) -> Long.compare(n1.getID(), n2.getID());
	private static final Comparator<POI> POI_ALPH_COMP = (p1, p2) -> p1
		.getLabel().compareTo(p2.getLabel());
	private static final Comparator<Street> STREET_ALPH_COMP = (s1,
		s2) -> s1.getStreetName().compareTo(s2.getStreetName());

	private static final int NODE_LVL = 2;
	private static final int WAY_NODE_LVL = 4;

	private static final String COMMENT_BOUNDING_BOX = "Definition Bounding Box";
	private static final String COMMENT_POI = "Definition wichtige Punkte";
	private static final String COMMENT_NODES = "Definition der Knotenpunkte";
	private static final String COMMENT_WAYS = "Definition von Straßen/-segmenten";
	private static final String COMMENT_END = "END "
		+ OSMFormat.ROOT.toUpperCase();

	private Optional<BoundingBox> boundingBox;
	private boolean trunc;
	private SortedSet<POI> pois;
	private SortedSet<Street> streets;

	public DocBuilder() {
	    boundingBox = Optional.empty();
	    trunc = false;
	    pois = new TreeSet<>(POI_ALPH_COMP);
	    streets = new TreeSet<>(STREET_ALPH_COMP);
	}

	@Override
	public void setBoundingBox(BoundingBox box, boolean truncateNodes) {
	    boundingBox = Optional.of(box);
	    trunc = truncateNodes;
	}

	@Override
	public void addPOI(POI p) {
	    if (!pois.add(p)) {
		throw new IllegalArgumentException(
			"Duplicate element " + p.getLabel());
	    }
	}

	@Override
	public void addAllPOIs(Collection<POI> coll) {
	    if (!coll.isEmpty() && !pois.addAll(coll)) {
		throw new IllegalArgumentException("Duplicate elements!");
	    }
	}

	@Override
	public void addStreet(Street street) {
	    if (!streets.add(street)) {
		throw new IllegalArgumentException(
			"Duplicate street " + street.getStreetName());
	    }
	}

	@Override
	public void addAllStreets(Collection<Street> streetCollection) {
	    if (!streetCollection.isEmpty()
		    && !streets.addAll(streetCollection)) {
		throw new IllegalArgumentException("Duplicate elements!");
	    }
	}

	@Override
	public void writeDocument(XMLStreamWriter xml)
		throws XMLStreamException {
	    newline(xml);
	    xml.writeStartElement(OSMFormat.ROOT);
	    newline(xml);
	    // write bouding box, if available
	    if (boundingBox.isPresent()) {
		writeBB(xml);
	    }

	    // write pois
	    if (pois.size() > 0) {
		intend(xml, NODE_LVL);
		xml.writeComment(COMMENT_POI);
		newline(xml);
		for (POI poi : pois) {
		    writePOI(xml, poi);
		}
		newline(xml);
	    }

	    SortedSet<Street.Node> nodes = new TreeSet<>(NODE_ID_COMP);
	    SortedSet<TruncatedSegment> segments = truncate();
	    segments.forEach(s -> nodes.addAll(s.nodes));

	    // write nodes
	    if (nodes.size() > 0) {
		intend(xml, NODE_LVL);
		xml.writeComment(COMMENT_NODES);
		newline(xml);
		for (Street.Node node : nodes) {
		    writeNode(xml, node);
		}
		newline(xml);
	    }

	    // write streets
	    if (streets.size() > 0) {
		intend(xml, NODE_LVL);
		xml.writeComment(COMMENT_WAYS);
		newline(xml);
		for (TruncatedSegment seg : segments) {
		    writeSegment(xml, seg);
		}
		newline(xml);
	    }

	    xml.writeComment(COMMENT_END);
	    newline(xml);
	    xml.writeEndElement();// END OsmFormat.ROOT
	}

	private SortedSet<TruncatedSegment> truncate() {
	    SortedSet<TruncatedSegment> set = new TreeSet<>();// truncated
							      // segment
	    // implements comparable
	    if (!trunc) {
		for (Street street : streets) {
		    for (Street.Segment segment : street.getSegments()) {
			set.add(new TruncatedSegment(street, segment,
				segment.getNodes()));
		    }
		}
		return set;
	    } else {
		for (Street street : streets) {
		    for (Street.Segment segment : street.getSegments()) {
			for (List<Street.Node> nodes : truncate(segment)) {
			    set.add(new TruncatedSegment(street, segment,
				    nodes));
			}
		    }
		}
		return set;
	    }
	}

	private void writeBB(XMLStreamWriter xml) throws XMLStreamException {
	    BoundingBox box = boundingBox.get();
	    intend(xml, NODE_LVL);
	    xml.writeComment(COMMENT_BOUNDING_BOX);
	    newline(xml);
	    intend(xml, NODE_LVL);
	    xml.writeStartElement(OSMFormat.BOUNDING_BOX);
	    xml.writeAttribute(OSMFormat.ATTR_MINLAT,
		    Double.toString(box.minLatitude));
	    xml.writeAttribute(OSMFormat.ATTR_MINLON,
		    Double.toString(box.minLongitude));
	    xml.writeAttribute(OSMFormat.ATTR_MAXLAT,
		    Double.toString(box.maxLatitude));
	    xml.writeAttribute(OSMFormat.ATTR_MAXLON,
		    Double.toString(box.maxLongitude));
	    xml.writeEndElement();
	    newline(xml);
	    newline(xml);
	}

	private void writePOI(XMLStreamWriter xml, POI poi)
		throws XMLStreamException {
	    LatLong coordinates = poi.getCoordinates();
	    intend(xml, NODE_LVL);
	    xml.writeStartElement(OSMFormat.POI);
	    xml.writeAttribute(OSMFormat.ATTR_LAT,
		    Double.toString(coordinates.getLatitude()));
	    xml.writeAttribute(OSMFormat.ATTR_LONG,
		    Double.toString(coordinates.getLongitude()));
	    xml.writeAttribute(OSMFormat.ATTR_POI_LABEL, poi.getLabel());
	    xml.writeEndElement();
	    newline(xml);
	}

	private void writeNode(XMLStreamWriter xml, Street.Node node)
		throws XMLStreamException {
	    intend(xml, NODE_LVL);
	    xml.writeStartElement(OSMFormat.NODE);
	    xml.writeAttribute(OSMFormat.ATTR_ID, Long.toString(node.getID()));
	    xml.writeAttribute(OSMFormat.ATTR_LAT,
		    Double.toString(node.getCoordinates().latitude));
	    xml.writeAttribute(OSMFormat.ATTR_LONG,
		    Double.toString(node.getCoordinates().longitude));
	    xml.writeEndElement();
	    newline(xml);
	}

	private List<List<Street.Node>> truncate(Street.Segment seg) {
	    BoundingBox bb = boundingBox.get();
	    List<List<Street.Node>> list = new ArrayList<>();
	    List<Street.Node> current = null;
	    boolean lastInside = false;
	    for (Street.Node node : seg.getNodes()) {
		if (bb.contains(node.getCoordinates())) {
		    if (current == null) {
			current = new ArrayList<>();
		    }
		    current.add(node);
		    lastInside = true;
		} else if (lastInside) {
		    current.add(node);
		    lastInside = false;
		} else if (current != null) {
		    list.add(current);
		    current = null;
		}
	    }
	    if (current != null) {
		list.add(current);
	    }
	    return list;
	}

	private void writeSegment(XMLStreamWriter xml, TruncatedSegment seg)
		throws XMLStreamException {
	    intend(xml, NODE_LVL);
	    xml.writeStartElement(OSMFormat.WAY);
	    newline(xml);
	    for (Street.Node node : seg.nodes) {
		intend(xml, WAY_NODE_LVL);
		xml.writeStartElement(OSMFormat.WAY_NODE);
		xml.writeAttribute(OSMFormat.ATTR_REF,
			Long.toString(node.getID()));
		xml.writeEndElement();
		newline(xml);
	    }
	    intend(xml, WAY_NODE_LVL);
	    writeTagNode(xml, OSMFormat.TAG_STREET_TYPE,
		    seg.segment.getHighwayType());
	    intend(xml, WAY_NODE_LVL);
	    writeTagNode(xml, OSMFormat.TAG_STREETNAME,
		    seg.street.getStreetName());
	    intend(xml, NODE_LVL);
	    xml.writeEndElement();
	    newline(xml);
	}

	private void intend(XMLStreamWriter xml, int lvl)
		throws XMLStreamException {
	    for (int i = 0; i < lvl; ++i) {
		xml.writeCharacters(" ");
	    }
	}

	private void newline(XMLStreamWriter xml) throws XMLStreamException {
	    xml.writeCharacters(System.getProperty("line.separator"));
	}

	private void writeTagNode(XMLStreamWriter xml, String key, String value)
		throws XMLStreamException {
	    xml.writeStartElement(OSMFormat.KEY_VALUE_NODE);
	    xml.writeAttribute(OSMFormat.ATTR_KEY, key);
	    xml.writeAttribute(OSMFormat.ATTR_VALUE, value);
	    xml.writeEndElement();
	    newline(xml);
	}
    }

    private static class TruncatedSegment
	    implements Comparable<TruncatedSegment> {
	public Street.Segment segment;
	public List<Street.Node> nodes;
	public Street street;

	public TruncatedSegment(Street s, Street.Segment seg,
		List<Street.Node> node) {
	    segment = seg;
	    nodes = node;
	    street = s;
	}

	@Override
	public int compareTo(TruncatedSegment o) {
	    // quick equality check
	    if (this == o) {
		return 0;
	    }
	    int streetCmp = street.getStreetName()
		    .compareTo(o.street.getStreetName());
	    if (streetCmp == 0) {
		int sizeCmp = Integer.compare(nodes.size(), o.nodes.size());
		if (sizeCmp == 0) {
		    Iterator<Street.Node> it1 = nodes.iterator();
		    Iterator<Street.Node> it2 = o.nodes.iterator();
		    while (it1.hasNext() && it2.hasNext()) {
			int nodeCmp = Long.compare(it1.next().getID(),
				it2.next().getID());
			if (nodeCmp != 0) {
			    return nodeCmp;
			}
		    }
		}
		return sizeCmp;
	    }
	    return streetCmp;

	}
    }
}
