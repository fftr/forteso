/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.Optional;

import org.mapsforge.core.model.BoundingBox;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import forteso.io.OSMFormat;
import forteso.io.OSMParsedDataProvider;
import forteso.io.OSMParserExtension;

class BoundingBoxExtension
	implements OSMParsedDataProvider<Optional<BoundingBox>> {

    private Optional<BoundingBox> boundingBox;

    BoundingBoxExtension() {
	boundingBox = Optional.empty();
    }

    @Override
    public void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException {
	if (OSMFormat.BOUNDING_BOX.equals(qName)) {
	    if (boundingBox.isPresent()) {
		throw new SAXException("Duplicate bounding box definition");
	    }
	    double minLat = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_MINLAT);
	    double minLon = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_MINLON);
	    double maxLat = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_MAXLAT);
	    double maxLon = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_MAXLON);
	    boundingBox = Optional
		    .of(new BoundingBox(minLat, minLon, maxLat, maxLon));
	}
    }

    @Override
    public void handleEndElement(String uri, String localName, String qName)
	    throws SAXException {
	// nothing to do
    }

    @Override
    public void parsingFinished() {

    }

    @Override
    public Optional<BoundingBox> getParsedData() {
	return boundingBox;
    }

}
