/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.mapsforge.core.model.LatLong;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import forteso.io.OSMFormat;
import forteso.io.OSMParsedDataProvider;
import forteso.io.OSMParserExtension;
import forteso.trainer.HouseNumber;
import forteso.trainer.Street;
import forteso.trainer.Street.Node;
import forteso.utils.LatLongCreator;

class HouseNumberExtension extends TagExtension
	implements OSMParsedDataProvider<Map<HouseNumber, LatLong>> {

    private Map<HouseNumber, List<Street.Node>> houseNrs;
    private List<Street.Node> currentWay;
    private long id;

    private NodeExtension nodes;

    HouseNumberExtension() {
	super(OSMFormat.TAG_HOUSENUMBER, OSMFormat.TAG_STREETADDRESS);
	houseNrs = new HashMap<>();
	currentWay = new ArrayList<>();
    }

    @Override
    public void setDependency(OSMParserExtension dep)
	    throws ParserConfigurationException {
	nodes = OSMParserExtension.checkedCast(dep);
    }

    @Override
    public void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException {
	super.handleStartElementstartElement(uri, localName, qName, attributes);
	switch (qName) {
	case OSMFormat.WAY_NODE:
	    handleWayNode(attributes);
	    break;
	case OSMFormat.NODE:
	    id = OSMParserExtension.parseLong(attributes, OSMFormat.ATTR_ID);
	    // fall-through
	case OSMFormat.WAY:
	    clear();
	    break;
	default:
	}
    }

    private void handleWayNode(Attributes a) throws SAXException {
	long id = OSMParserExtension.parseLong(a, OSMFormat.ATTR_REF);
	Street.Node node = nodes.getNode(id).orElseThrow(
		() -> new SAXException("Cannot find node for ref " + id));
	currentWay.add(node);
    }

    private void clear() {
	clearTags();
	currentWay.clear();
    }

    @Override
    public void handleEndElement(String uri, String localName, String qName)
	    throws SAXException {
	super.handleEndElement(uri, localName, qName);
	switch (qName) {
	case OSMFormat.NODE:
	    handleNode();
	    break;
	case OSMFormat.WAY:
	    handleWay();
	}
    }

    private void handleNode() throws SAXException {
	Optional<String> hNr = getTag(OSMFormat.TAG_HOUSENUMBER);
	Optional<String> addrStreet = getTag(OSMFormat.TAG_STREETADDRESS);
	if (hNr.isPresent() && addrStreet.isPresent()) {
	    Street.Node node = nodes.getNode(id).orElseThrow(
		    () -> new SAXException("Cannot find node for ref " + id));
	    add(addrStreet.get(), hNr.get(), node);
	}
    }

    private void handleWay() {
	Optional<String> hNr = getTag(OSMFormat.TAG_HOUSENUMBER);
	Optional<String> addrStreet = getTag(OSMFormat.TAG_STREETADDRESS);
	if (hNr.isPresent() && addrStreet.isPresent()) {

	    add(addrStreet.get(), hNr.get(), currentWay);
	}
    }

    private void add(String addrStreet, String hNr, Node node) {
	currentWay.add(node);
	add(addrStreet, hNr, currentWay);
    }

    private void add(String addrStreet, String hNr, List<Node> nodes) {
	HouseNumber key = new HouseNumber(addrStreet, hNr);
	List<Node> houseNodes = houseNrs.computeIfAbsent(key,
		k -> new LinkedList<>());
	houseNodes.addAll(nodes);
    }

    @Override
    public void parsingFinished() {
	super.parsingFinished();
    }

    @Override
    public Map<HouseNumber, LatLong> getParsedData() {
	Map<HouseNumber, LatLong> result = new HashMap<>(houseNrs.size());
	for (Entry<HouseNumber, List<Node>> entry : houseNrs.entrySet()) {
	    Optional<LatLong> latlong = entry.getValue().stream()
		    .map(Node::getCoordinates).reduce(this::reduce);
	    latlong.ifPresent(coord -> result.put(entry.getKey(), coord));
	}
	return result;
    }

    private LatLong reduce(LatLong acc, LatLong node) {
	// it is ok to use an unchecked ctor here, because a average operation
	// over legal coordinates will never result to an illegal coordinate
	return LatLongCreator.createUnchecked(avg(acc.latitude, node.latitude),
		avg(acc.longitude, node.longitude));
    }

    private double avg(double d1, double d2) {
	return (d1 + d2) / 2;
    }
}
