/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import forteso.io.OSMFormat;
import forteso.io.OSMParsedDataProvider;
import forteso.io.OSMParserExtension;
import forteso.trainer.POI;
import forteso.utils.IllegalCoordinateException;

class POIExtension implements OSMParsedDataProvider<Collection<POI>> {

    private Map<String, POI> pois;

    POIExtension() {
	pois = new HashMap<>();
    }

    @Override
    public void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException {
	if (OSMFormat.POI.equals(qName)) {
	    double lat = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_LAT);
	    double lon = OSMParserExtension.parseDouble(attributes,
		    OSMFormat.ATTR_LONG);
	    String label = OSMParserExtension.parseString(attributes,
		    OSMFormat.ATTR_POI_LABEL);
	    if (pois.containsKey(label)) {
		throw new SAXException(
			"Duplicate definition of POI with label " + label);
	    }
	    try {
		pois.put(label, new POI(lat, lon, label));
	    } catch (IllegalCoordinateException e) {
		throw new SAXException(e);
	    }

	}
    }

    @Override
    public void handleEndElement(String uri, String localName, String qName)
	    throws SAXException {
	// nothing to do here
    }

    @Override
    public Collection<POI> getParsedData() {
	return Collections.unmodifiableCollection(pois.values());
    }

    @Override
    public void parsingFinished() {

    }
}
