/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Supplier;

import javax.xml.parsers.ParserConfigurationException;

import forteso.io.OSMParsedDataProvider;
import forteso.io.OSMParserExtension;
import forteso.utils.CollectionUtils;

public enum ParserModule {

    // @formatter:off
    NODES(NodeExtension::new,0), BOUNDING_BOX(1, BoundingBoxExtension::new), 
    HOME_MARKER(2, HomeMarkerExtension::new), 
    STREETS(3, StreetExtension::new, Optional.of(NODES), Optional.empty()), 
    POIS(4, POIExtension::new), 
    HOUSE_NUMBERS(5, HouseNumberExtension::new, Optional.of(NODES), Optional.empty());
    // @formatter:on

    private Supplier<OSMParserExtension> parser;
    private Supplier<OSMParsedDataProvider<?>> dataProvider;
    private int priority;
    private Optional<ParserModule> dependency;
    private Optional<ParserModule> optDependency;

    private ParserModule(int prio, Supplier<OSMParsedDataProvider<?>> fact,
	    Optional<ParserModule> dep, Optional<ParserModule> optDep,
	    Supplier<OSMParserExtension> pars) {
	priority = prio;
	parser = pars;
	dataProvider = fact;
	dependency = Objects.requireNonNull(dep);
	optDependency = Objects.requireNonNull(optDep);
    }

    private ParserModule(int prio, Supplier<OSMParsedDataProvider<?>> fact,
	    Optional<ParserModule> dep, Optional<ParserModule> optDep) {
	this(prio, fact, dep, optDep, null);
    }

    private ParserModule(int prio, Supplier<OSMParsedDataProvider<?>> fact) {
	this(prio, fact, Optional.empty(), Optional.empty());
    }

    private ParserModule(Supplier<OSMParserExtension> ext, int prio) {
	this(prio, null, Optional.empty(), Optional.empty(),
		Objects.requireNonNull(ext));
    }

    private OSMParsedDataProvider<?> newProvider() {
	return dataProvider != null ? dataProvider.get() : null;
    }

    private OSMParserExtension newExtension() {
	return parser != null ? parser.get() : null;
    }

    private Optional<ParserModule> getDependency() {
	return dependency;
    }

    private Optional<ParserModule> getOptionalDependency() {
	return optDependency;
    }

    private static Comparator<ParserModule> priorityComparator() {
	return (ParserModule m1, ParserModule m2) -> Integer
		.compare(m1.priority, m2.priority);
    }

    public static ParserExtensionBundle createConfiguration(
	    ParserModule... modules) throws ParserConfigurationException {
	return new Bundle(modules);
    }

    private static class Bundle implements ParserExtensionBundle {
	private Map<ParserModule, OSMParsedDataProvider<?>> bundle;
	private SortedMap<ParserModule, OSMParserExtension> extensions;

	public Bundle(ParserModule[] modules)
		throws ParserConfigurationException {
	    bundle = new TreeMap<>();
	    extensions = new TreeMap<>(priorityComparator());
	    resolveAndLoad(modules);
	}

	private SortedSet<ParserModule> buildTransitiveHull(
		ParserModule[] modules) throws ParserConfigurationException {
	    SortedSet<ParserModule> hull = new TreeSet<>(priorityComparator());
	    Queue<ParserModule> q = new LinkedList<>();
	    CollectionUtils.addAll(q, modules);
	    while (!q.isEmpty()) {
		ParserModule module = q.poll();
		hull.add(module);
		module.getDependency().ifPresent(dep -> {
		    if (!q.contains(dep) && !hull.contains(dep)) {
			q.add(dep);
		    }
		});
		module.getOptionalDependency().ifPresent(dep -> {
		    if (!q.contains(dep) && !hull.contains(dep)) {
			q.add(dep);
		    }
		});
	    }
	    return hull;
	}

	private void resolveAndLoad(ParserModule[] mods)
		throws ParserConfigurationException {
	    SortedSet<ParserModule> modules = buildTransitiveHull(mods);
	    for (ParserModule module : modules) {
		OSMParsedDataProvider<?> provider = module.newProvider();
		OSMParserExtension ext = provider != null ? provider
			: module.newExtension();
		CollectionUtils.ifPresentChecked(module.getDependency(),
			dependency -> {
			    OSMParserExtension depExtension = extensions
				    .get(dependency);
			    if (depExtension != null) {
				ext.setDependency(depExtension);
			    }
			});
		CollectionUtils.ifPresentChecked(module.getOptionalDependency(),
			dependency -> {
			    OSMParserExtension optExtension = extensions
				    .get(dependency);
			    if (optExtension != null) {
				ext.setOptionalDependency(optExtension);
			    }
			});
		if (extensions.put(module, ext) != null) {
		    throw new ParserConfigurationException(
			    "Duplicate module " + module.name());
		}
		if (provider != null) {
		    bundle.put(module, provider);
		}
	    }
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getParsedData(ParserModule module)
		throws ParserConfigurationException {
	    OSMParsedDataProvider<?> ext = bundle.get(module);
	    if (ext == null) {
		throw new ParserConfigurationException(
			"No extension for module " + module.name() + " found");
	    }
	    try {
		return (T) ext.getParsedData();
	    } catch (ClassCastException e) {
		throw new ParserConfigurationException("For date of module "
			+ module.name() + "\n" + e.getMessage());
	    }
	}

	@Override
	public Collection<OSMParserExtension> getExtensions() {
	    return Collections.unmodifiableCollection(extensions.values());
	}

    }
}
