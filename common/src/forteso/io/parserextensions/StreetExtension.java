/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import forteso.io.OSMFormat;
import forteso.io.OSMParsedDataProvider;
import forteso.io.OSMParserExtension;
import forteso.trainer.Street;
import forteso.utils.CollectionUtils;

class StreetExtension extends TagExtension
	implements OSMParsedDataProvider<Collection<Street>> {

    private List<Long> nodes;

    private Map<String, Street> streets;

    private NodeExtension allNodes;

    StreetExtension() {
	super(OSMFormat.TAG_STREET_TYPE, OSMFormat.TAG_STREETNAME);
	streets = new HashMap<>();
	nodes = new ArrayList<>();
    }

    @Override
    public void setDependency(OSMParserExtension dep)
	    throws ParserConfigurationException {
	allNodes = OSMParserExtension.checkedCast(dep);
    }

    @Override
    public void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException {
	super.handleStartElementstartElement(uri, localName, qName, attributes);
	switch (qName) {
	case OSMFormat.WAY:
	    beginWay();
	    break;
	case OSMFormat.WAY_NODE:
	    addWayNode(attributes);
	    break;
	default:

	}
    }

    @Override
    public void handleEndElement(String uri, String localName, String qName)
	    throws SAXException {
	super.handleEndElement(uri, localName, qName);
	if (qName.equals(OSMFormat.WAY)) {
	    endWay();
	}
    }

    private void beginWay() {
	clearTags();
	nodes.clear();
    }

    private void addWayNode(Attributes a) throws SAXException {
	Long id = OSMParserExtension.parseLong(a, OSMFormat.ATTR_REF);
	nodes.add(id);
    }

    private void endWay() throws SAXException {
	Optional<String> highwayType = getTag(OSMFormat.TAG_STREET_TYPE);
	Optional<String> name = getTag(OSMFormat.TAG_STREETNAME);
	if (highwayType.isPresent() && name.isPresent()
		&& CollectionUtils.anyOf(highwayType.get()::equals,
			OSMFormat.VALID_STREET_TYPES)) {

	    Street street = streets.computeIfAbsent(name.get(), Street::new);
	    street.addSegment(
		    new Street.Segment(getNodeSegment(), highwayType.get()));
	}
    }

    private List<Street.Node> getNodeSegment() throws SAXException {
	List<Street.Node> streetNodes = new ArrayList<>(nodes.size());
	for (Long id : nodes) {
	    Street.Node node = allNodes.getNode(id).orElseThrow(
		    () -> new SAXException("Cannot find node for ref " + id));
	    streetNodes.add(node);
	}
	return streetNodes;
    }

    @Override
    public Collection<Street> getParsedData() {
	return Collections.unmodifiableCollection(streets.values());
    }

    @Override
    public void parsingFinished() {
	super.parsingFinished();
	// TODO evtl exception wenn aktuelle straße nicht abgeschlossen
    }
}
