/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.io.parserextensions;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import forteso.io.OSMFormat;
import forteso.io.OSMParserExtension;
import forteso.utils.CollectionUtils;

abstract class TagExtension implements OSMParserExtension {

    private Map<String, String> tags;
    private final Set<String> keysToSave;

    TagExtension(String... keys) {
	tags = new TreeMap<>();
	keysToSave = new TreeSet<>();
	CollectionUtils.addAll(keysToSave, keys);
    }

    void clearTags() {
	tags.clear();
    }

    Optional<String> getTag(String key) {
	return Optional.ofNullable(tags.get(key));
    }

    @Override
    public void handleStartElementstartElement(String uri, String localName,
	    String qName, Attributes attributes) throws SAXException {
	if (OSMFormat.KEY_VALUE_NODE.equals(qName)) {
	    String k = attributes.getValue(OSMFormat.ATTR_KEY);
	    if (keysToSave.contains(k)) {
		String v = attributes.getValue(OSMFormat.ATTR_VALUE);
		if (k == null || v == null) {
		    throw new SAXException(
			    "Tag does not contain key-value pair ("
				    + OSMFormat.ATTR_KEY + "=" + k + ", "
				    + OSMFormat.ATTR_VALUE + "=" + v);
		}
		tags.put(k, v);
	    }
	}
    }

    @Override
    public void handleEndElement(String uri, String localName, String qName)
	    throws SAXException {

    }

    @Override
    public void parsingFinished() {

    }

}
