/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Objects;

import org.mapsforge.core.model.LatLong;

public class FindResult {

    private FindableEntity entity;
    private LatLong tapLocation;
    private LatLong nearestStreetNode;

    public FindResult(LatLong tap, LatLong nearest, FindableEntity f) {
	tapLocation = Objects.requireNonNull(tap, () -> "Location is null");
	nearestStreetNode = Objects.requireNonNull(nearest,
		() -> "Nearest street node is null");
	entity = Objects.requireNonNull(f, () -> "FindableEntity is null");
    }

    public LatLong getTap() {
	return tapLocation;
    }

    public LatLong getNearestNode() {
	return nearestStreetNode;
    }

    public FindableEntity getEntity() {
	return entity;
    }

}
