/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Objects;

public class HouseNumber implements Comparable<HouseNumber> {
    private final String streetName;
    private final String houseNumber;

    public HouseNumber(String street, String number) {
	streetName = Objects.requireNonNull(street);
	houseNumber = Objects.requireNonNull(number);
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + houseNumber.hashCode();
	result = prime * result + streetName.hashCode();
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	HouseNumber other = (HouseNumber) obj;
	if (houseNumber == null) {
	    if (other.houseNumber != null)
		return false;
	} else if (!houseNumber.equals(other.houseNumber))
	    return false;
	if (streetName == null) {
	    if (other.streetName != null)
		return false;
	} else if (!streetName.equals(other.streetName))
	    return false;
	return true;
    }

    @Override
    public int compareTo(HouseNumber o) {
	int streetCmp = streetName.compareTo(o.streetName);
	return streetCmp != 0 ? streetCmp
		: houseNumber.compareTo(o.houseNumber);
    }

    @Override
    public String toString() {
	return streetName + ": " + houseNumber;
    }

    public String getStreetName() {
	return streetName;
    }

    public String getNumber() {
	return houseNumber;
    }

}