/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Objects;

import org.mapsforge.core.model.LatLong;

import forteso.utils.IllegalCoordinateException;
import forteso.utils.LatLongCreator;
import forteso.visualization.Markertype;
import forteso.visualization.OverlayManager;

public class POI implements FindableEntity {

    private LatLong coordinates;
    private String label;

    public POI(double lat, double lon, String label)
	    throws IllegalCoordinateException {
	this(LatLongCreator.createChecked(lat, lon), label);
    }

    public POI(LatLong coords, String label) {
	coordinates = Objects.requireNonNull(coords,
		() -> "Coordinates may not be null");
	this.label = Objects.requireNonNull(label,
		() -> "Label may not be null");
	if (label.trim().isEmpty()) {
	    throw new IllegalArgumentException("Label may not be empty");
	}
    }

    @Override
    public LatLong getNearest(LatLong point) {
	return coordinates;
    }

    @Override
    public void visualize(OverlayManager overlay) {
	overlay.addMarker(OverlayManager.GROUP_HIGHLIGHT_SEARCH_RES,
		coordinates, Markertype.BLUE);
    }

    @Override
    public String getKey() {
	return label;
    }

    public LatLong getCoordinates() {
	return coordinates;
    }

    public String getLabel() {
	return label;
    }

}
