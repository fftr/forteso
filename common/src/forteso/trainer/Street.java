/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.util.LatLongUtils;

import forteso.utils.CollectionUtils;
import forteso.visualization.OverlayManager;

public class Street implements FindableEntity {

    private List<Segment> segments;
    private String name;

    public Street(String name) {
	segments = new LinkedList<>();
	this.name = Objects.requireNonNull(name, () -> "Name may not be null");
	if (name.isEmpty()) {
	    throw new IllegalArgumentException("Name may not be empty");
	}
    }

    public void addSegment(Segment s) {
	if (segments.contains(
		Objects.requireNonNull(s, () -> "Node may not be null"))) {
	    throw new IllegalArgumentException("Duplicate " + s);
	}
	segments.add(s);
    }

    public void addAllSegments(Collection<? extends Segment> coll) {
	Objects.requireNonNull(coll, () -> "The collection may not be null")
		.forEach(this::addSegment);
    }

    @Override
    public int hashCode() {
	return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
	if (o == null || !(o instanceof Street)) {
	    return false;
	}
	return ((Street) o).name.equals(name);
    }

    public List<Segment> getSegments() {
	return Collections.unmodifiableList(segments);
    }

    public String getStreetName() {
	return name;
    }

    @Override
    public void visualize(OverlayManager overlay) {
	for (Segment seg : segments) {
	    List<LatLong> segmentPoints = seg.getNodes().stream()
		    .map(n -> n.getCoordinates()).collect(Collectors.toList());
	    overlay.addLine(OverlayManager.GROUP_HIGHLIGHT_SEARCH_RES,
		    segmentPoints, OverlayManager.STREET_PAINT);
	}
    }

    @Override
    public String getKey() {
	return getStreetName();
    }

    // public Node getNearest(LatLong point) {
    // Optional<Node> nearest = segments.stream().flatMap(segment ->
    // segment.nodes.stream())
    // .max((n1, n2) -> (int) (LatLongUtils.distance(point, n1.getCoordinates())
    // - LatLongUtils.distance(point, n2.getCoordinates())));
    // return nearest.orElseThrow(() -> new RuntimeException("Street has no
    // street nodes!"));
    // }

    @Override
    public LatLong getNearest(LatLong point) {
	if (point == null) {
	    throw new IllegalArgumentException(
		    "The latlong point may not be null");
	}
	Node minNode = null;
	for (Segment s : segments) {
	    for (Node n : s.nodes) {
		if (minNode == null) { // First Node
		    minNode = n;
		} else {
		    if (LatLongUtils.vincentyDistance(point,
			    n.getCoordinates()) < LatLongUtils.vincentyDistance(
				    point, minNode.getCoordinates()))
			minNode = n; // n was smaller
		}
	    }
	}

	if (minNode != null) {
	    return minNode.getCoordinates();
	} else {
	    throw new RuntimeException("Street has no street nodes!");
	}
    }

    public static class Segment {
	private List<Node> nodes;
	private String highwayType;
	private long start;
	private long end;

	public Segment(List<Node> n, String type) {
	    this.nodes = Objects.requireNonNull(n,
		    () -> "The node list may not be null");
	    if (nodes.size() < 1) {
		throw new IllegalArgumentException("Segment is empty");
	    }
	    highwayType = Objects.requireNonNull(type,
		    () -> "Type may not be null");
	    start = nodes.get(0).getID();
	    end = nodes.get(Math.max(0, nodes.size() - 1)).getID();
	}

	public List<Node> getNodes() {
	    return Collections.unmodifiableList(nodes);
	}

	public String getHighwayType() {
	    return highwayType;
	}

	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + (int) (end ^ (end >>> 32));
	    result = prime * result + (int) (start ^ (start >>> 32));
	    return result;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    Segment other = (Segment) obj;
	    return CollectionUtils.deepEquals(nodes, other.nodes);
	}

	@Override
	public String toString() {
	    return "Segment(" + start + " -> " + end + ")";
	}

    }

    public static class Node {
	private LatLong coordinates;
	private long id;

	public Node(long id, LatLong coord) {
	    this.id = id;
	    coordinates = Objects.requireNonNull(coord,
		    () -> "Coordinates may not be null");
	}

	public long getID() {
	    return id;
	}

	public LatLong getCoordinates() {
	    return coordinates;
	}

	@Override
	public int hashCode() {
	    return (int) id;
	}

	@Override
	public boolean equals(Object o) {
	    if (o == null || !(o instanceof Node)) {
		return false;
	    }
	    return equals((Node) o);
	}

	public boolean equals(Node n) {
	    return id == n.id;
	}
    }
}
