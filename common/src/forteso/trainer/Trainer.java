/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Objects;
import java.util.Optional;

import org.mapsforge.core.model.LatLong;

public class Trainer implements ModelInterface {

    private TrainingArea area;

    public Trainer(TrainingArea a) {
	area = Objects.requireNonNull(a, () -> "Training Area may not be null");
    }

    public TrainingArea getArea() {
	return area;
    }

    @Override
    public Optional<String> getRandomEntityKey() {
	return area.getRandomKey();
    }

    @Override
    public FindResult evaluateTap(String keyToFind, LatLong tapLocation) {
	FindableEntity findable = area.getFindableForKey(keyToFind).orElseThrow(
		() -> new IllegalArgumentException("Unknown key " + keyToFind));
	return new FindResult(tapLocation, findable.getNearest(tapLocation),
		findable);
    }

    @Override
    public void useStreets(boolean use) {
	area.useStreets(use);
    }

    @Override
    public void usePOIs(boolean use) {
	area.usePOIs(use);
    }

    @Override
    public boolean useStreets() {
	return area.useStreets();
    }

    @Override
    public boolean usePOIs() {
	return area.usePOIs();
    }

    @Override
    public Optional<LatLong> getHome() {
	return area.getHome();
    }

}
