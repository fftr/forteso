/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.trainer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;

import org.mapsforge.core.model.BoundingBox;
import org.mapsforge.core.model.LatLong;

import forteso.utils.CollectionUtils;
import forteso.utils.CombinatedIterator;

public class TrainingArea {

    private Map<String, Street> streets;
    private Map<String, POI> pointsOfInterest;
    private boolean useStreets;
    private boolean usePOIs;
    private BoundingBox boundingBox;
    private Optional<LatLong> home;

    public TrainingArea(BoundingBox box, Optional<LatLong> home) {
	boundingBox = Objects.requireNonNull(box,
		() -> "The bounding box may not be null");
	this.home = Objects.requireNonNull(home,
		() -> "May not be null, use Optional.empty instead");
	streets = new HashMap<>();
	pointsOfInterest = new HashMap<>();
	usePOIs = true;
	useStreets = true;
    }

    public Optional<LatLong> getHome() {
	return home;
    }

    public void useStreets(boolean use) {
	useStreets = use;
    }

    public void usePOIs(boolean use) {
	usePOIs = use;
    }

    public boolean useStreets() {
	return useStreets;
    }

    public boolean usePOIs() {
	return usePOIs;
    }

    public void addStreet(Street s) {
	checkDuplicate(
		Objects.requireNonNull(s, () -> "Street is null").getKey());
	streets.put(s.getStreetName(), s);

    }

    public void addPOI(POI p) {
	checkDuplicate(Objects.requireNonNull(p, () -> "POI is null").getKey());
	pointsOfInterest.put(p.getKey(), p);
    }

    private void checkDuplicate(String key) {
	if (streets.containsKey(key)) {
	    throw new IllegalArgumentException(
		    "A street with name " + key + " already exists");
	}
	if (pointsOfInterest.containsKey(key)) {
	    throw new IllegalArgumentException(
		    "A POI with name " + key + " already exists");
	}
    }

    public Optional<String> getRandomKey() {
	Random rnd = new Random();
	CombinatedIterator<FindableEntity> it = new CombinatedIterator<>();
	int elementCounter = 0;
	if (useStreets) {
	    it.add(streets.values());
	    elementCounter += streets.size();
	}
	if (usePOIs) {
	    it.add(pointsOfInterest.values());
	    elementCounter += pointsOfInterest.size();
	}

	if (elementCounter <= 0) {
	    return Optional.empty();
	}
	int skip = rnd.nextInt(elementCounter);
	return CollectionUtils.toStream(it, false).skip(skip).findFirst()
		.map(findable -> findable.getKey());
    }

    public Optional<Street> getStreetForName(String s) {
	return Optional.ofNullable(streets.get(s));
    }

    public Collection<String> getStreetNames() {
	return Collections.unmodifiableCollection(streets.keySet());
    }

    public Collection<Street> getStreets() {
	return Collections.unmodifiableCollection(streets.values());
    }

    public Collection<POI> getPOIs() {
	return Collections.unmodifiableCollection(pointsOfInterest.values());
    }

    public BoundingBox getBoundingBox() {
	return boundingBox;
    }

    public Optional<FindableEntity> getFindableForKey(String key) {
	Street s = streets.get(key);
	if (s != null) {
	    return Optional.of(s);
	}
	return Optional.ofNullable(pointsOfInterest.get(key));
    }

}
