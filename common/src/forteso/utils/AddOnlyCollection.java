/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.function.Consumer;

public interface AddOnlyCollection<T> extends Iterable<T> {

    void add(T element);

    default void addAll(Collection<? extends T> elements) {
	Objects.requireNonNull(elements).stream().forEach(this::add);
    }

    public static <T> AddOnlyCollection<T> wrapAsAddOnly(Collection<T> coll) {
	final Collection<T> collection = Objects.requireNonNull(coll);
	return new AddOnlyCollection<T>() {

	    @Override
	    public void add(T element) {
		collection.add(element);
	    }

	    @Override
	    public void addAll(Collection<? extends T> elements) {
		collection.addAll(elements);
	    }

	    @Override
	    public Iterator<T> iterator() {
		return collection.iterator();
	    }

	    @Override
	    public void forEach(Consumer<? super T> action) {
		collection.forEach(action);
	    }

	    @Override
	    public Spliterator<T> spliterator() {
		return collection.spliterator();
	    }

	};
    }
}
