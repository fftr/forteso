/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CollectionUtils {

    public static <T> boolean deepEquals(Collection<T> c1, Collection<T> c2) {
	if (Objects.requireNonNull(c1.size(),
		() -> "Collection1 is null") != Objects.requireNonNull(
			c2.size(), () -> "Collection2 is null")) {
	    return false;
	}
	Iterator<T> it1 = c1.iterator();
	Iterator<T> it2 = c2.iterator();
	while (it1.hasNext() && it2.hasNext()) {
	    if (!it1.next().equals(it2.next())) {
		return false;
	    }
	}
	return !it1.hasNext() && !it2.hasNext();
    }

    public static <T> boolean anyOf(Predicate<? super T> pred, T[] a) {
	for (T t : a) {
	    if (pred.test(t)) {
		return true;
	    }
	}
	return false;
    }

    public static <T> Stream<T> toStream(Iterator<T> it, boolean parallel) {
	Objects.requireNonNull(it, () -> "The iterator may not be null");
	Iterable<T> itable = () -> it;
	return StreamSupport.stream(itable.spliterator(), parallel);
    }

    public static <T, U, E extends Exception> Optional<U> mapChecked(
	    Optional<T> opt, CheckedMapFunction<T, U, E> map) throws E {
	if (opt.isPresent()) {
	    return Optional.ofNullable(map.apply(opt.get()));
	}
	return Optional.empty();
    }

    public interface CheckedMapFunction<T, U, E extends Exception> {

	U apply(T arg) throws E;
    }

    public static <T, E extends Exception> void ifPresentChecked(
	    Optional<T> opt, CheckedConsumer<? super T, E> consumer) throws E {
	if (opt.isPresent()) {
	    consumer.accept(opt.get());
	}
    }

    public static <T> void addAll(Collection<? super T> collection, T[] toAdd) {
	for (int i = 0; i < toAdd.length; i++) {
	    collection.add(toAdd[i]);
	}
    }

    public interface CheckedConsumer<T, E extends Exception> {
	void accept(T t) throws E;
    }

    public static <T, E extends Exception> void forEachChecked(
	    Collection<T> collection, CheckedConsumer<T, E> action) throws E {
	for (T t : collection) {
	    action.accept(t);
	}
    }
}
