/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

public class CombinatedIterator<T> implements Iterator<T> {
    private Queue<Iterator<? extends T>> iterators;
    private Iterator<? extends T> it;

    public CombinatedIterator() {
	iterators = new LinkedList<>();
    }

    public void add(Iterable<? extends T> iterable) {
	if (it == null) {
	    it = iterable.iterator();
	} else {
	    iterators.add(iterable.iterator());
	}
    }

    @Override
    public boolean hasNext() {
	if (it != null && it.hasNext()) {
	    return true;
	}
	do {
	    it = iterators.poll();
	} while ((it != null) && !it.hasNext());
	return it != null;
    }

    @Override
    public T next() {
	if (it != null && it.hasNext()) {
	    return it.next();
	}
	do {
	    it = iterators.poll();
	} while (it != null && !it.hasNext());
	if (it != null) {
	    return it.next();
	} else {
	    throw new NoSuchElementException("No element remaining");
	}
    }

}
