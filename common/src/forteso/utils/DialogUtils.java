/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Optional;

import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;

public class DialogUtils {

    private DialogUtils() {

    }

    public static void showBlockingErrorDialog(String msg, Throwable t) {
	Dialog<String> dialog = new Dialog<>();
	ByteArrayOutputStream os = new ByteArrayOutputStream();
	PrintWriter pw = new PrintWriter(os, true);
	t.printStackTrace(pw);
	Label label = new Label(new String(os.toByteArray()));
	label.setMinSize(label.getPrefWidth(), label.getPrefHeight());
	label.setWrapText(true);
	dialog.setContentText(msg);
	ScrollPane pane = new ScrollPane();
	pane.setFitToHeight(true);
	pane.setContent(label);
	dialog.getDialogPane().setExpandableContent(pane);
	dialog.setTitle("Error");
	ButtonType dismiss = new ButtonType("Ok", ButtonData.OK_DONE);
	dialog.getDialogPane().getButtonTypes().add(dismiss);
	dialog.showAndWait();
    }

    public static void showInfoDialog(String title, String msg) {
	Dialog<String> dialog = new Dialog<>();
	dialog.setTitle(title);
	dialog.setContentText(msg);
	ButtonType dismiss = new ButtonType("Ok", ButtonData.OK_DONE);
	dialog.getDialogPane().getButtonTypes().add(dismiss);
	dialog.show();
    }

    public static boolean showBlockingYesNoDialog(String title, String msg) {
	Dialog<ButtonType> dialog = new Dialog<>();
	dialog.setTitle(title);
	dialog.setContentText(msg);
	ButtonType confirm = new ButtonType("Ja", ButtonData.YES);
	ButtonType abort = new ButtonType("Nein", ButtonData.NO);
	dialog.getDialogPane().getButtonTypes().add(confirm);
	dialog.getDialogPane().getButtonTypes().add(abort);
	Optional<ButtonType> button = dialog.showAndWait();

	return button.filter(type -> type.getButtonData() == ButtonData.YES)
		.isPresent();
    }

}
