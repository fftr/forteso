/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import org.mapsforge.core.model.LatLong;
import org.xml.sax.SAXException;

public final class LatLongCreator {

    private LatLongCreator() {
	// not intended to instantiate
    }

    public static LatLong createChecked(double lat, double lon)
	    throws IllegalCoordinateException {
	try {
	    return new LatLong(lat, lon);
	} catch (IllegalArgumentException e) {
	    throw new IllegalCoordinateException(e);
	}
    }

    public static LatLong createSAX(double lat, double lon)
	    throws SAXException {
	try {
	    return new LatLong(lat, lon);
	} catch (IllegalArgumentException e) {
	    throw new SAXException(e);
	}
    }

    public static LatLong createUnchecked(double lat, double lon)
	    throws IllegalArgumentException {
	return new LatLong(lat, lon);
    }

}
