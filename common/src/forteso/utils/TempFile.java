/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

public class TempFile extends File implements AutoCloseable {

    private static final long serialVersionUID = -8864484436277439027L;

    public TempFile(String pathname) {
	super(pathname);
    }

    public TempFile(URI uri) {
	super(uri);
    }

    public TempFile(String parent, String child) {
	super(parent, child);
    }

    public TempFile(File parent, String child) {
	super(parent, child);
    }

    @Override
    public void close() throws IOException {
	Files.deleteIfExists(this.toPath());
    }

    public static TempFile createTempFile(String prefix, String suffix,
	    FileAttribute<?>... attrs) throws IOException {
	// just to get the platform dependent temp directory + naming
	Path filePath = Files.createTempFile(prefix, suffix, attrs);
	return new TempFile(filePath.toUri());
    }

    public static TempFile createTempFile(String prefix, String suffix)
	    throws IOException {

	return createTempFile(prefix, suffix, new FileAttribute<?>[0]);
    }
}
