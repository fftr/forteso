/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.visualization;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

public enum Markertype {

    //@formatter:off
	OLD("/resources/bitmaps/old.bmp", 0, -17), 
	RED("/resources/bitmaps/red.bmp", 0, -14), 
	GREEN("/resources/bitmaps/green.bmp", 0, -14), 
	ORANGE("/resources/bitmaps/orange.bmp", 0, -14), 
	BLUE("/resources/bitmaps/blue.bmp", 0, -14),
    	HOME("/resources/bitmaps/home.bmp", 0, -14);
    //@formatter:on

    private URL url;
    private int horizontalOffset;
    private int verticalOffset;

    private Markertype(String path, int horizontalOffset, int verticalOffset) {
	url = Objects.requireNonNull(Markertype.class.getResource(path),
		() -> "Resource path " + path + " not found");
	this.horizontalOffset = horizontalOffset;
	this.verticalOffset = verticalOffset;
    }

    public InputStream openStream() throws IOException {
	return url.openStream();
    }

    public int getHorizontalOffset() {
	return horizontalOffset;
    }

    public int getVerticalOffset() {
	return verticalOffset;
    }

}
