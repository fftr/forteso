/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * Forteso is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * Forteso is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with
 * Forteso. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * LPGL licensed Libraries:
 *   Mapsforge: Copyright 2010, 2011, 2012, 2013 mapsforge.org
 *   kXML: Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *   Browserlauncher: Copyright 2006 Jeff Chapman
 *   SVG Salamander: Copyright (c) 2004, Mark McKay
 *******************************************************************************/
package forteso.visualization;

import java.util.List;

import org.mapsforge.core.graphics.Cap;
import org.mapsforge.core.graphics.Color;
import org.mapsforge.core.graphics.Join;
import org.mapsforge.core.graphics.Paint;
import org.mapsforge.core.graphics.Style;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.map.awt.graphics.AwtGraphicFactory;
import org.mapsforge.map.layer.Layer;


public interface OverlayManager {
	
	public static final String GROUP_HOME = "group-home";
    public static final String GROUP_HIGHLIGHT_SEARCH_RES = "group-search";

    public static final Paint STREET_PAINT = getPaint(Color.BLUE, 5);
    public static final Paint DISTANCE_PAINT = getPaint(Color.BLACK, 2);
    
    public static Paint getPaint(Color c, float linewidth) {
    	Paint p = AwtGraphicFactory.INSTANCE.createPaint();
    	p.setColor(c);
    	p.setStrokeWidth(linewidth);
    	p.setStyle(Style.STROKE);
    	p.setStrokeCap(Cap.ROUND);
    	p.setStrokeJoin(Join.ROUND);
    	return p;
        }
	
	Layer addMarker(String group, LatLong latlon, Markertype type);
	
	Layer addLine(String group, List<LatLong> points, Paint p);
	
	void markDistance(String group, LatLong from, LatLong to);
	
	void removeAll();
	
	void remove(String group);

}
