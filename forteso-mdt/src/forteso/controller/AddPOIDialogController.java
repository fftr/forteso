/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.mapsforge.core.model.LatLong;

import forteso.trainer.HouseNumber;
import forteso.trainer.POI;
import forteso.uiutils.RadioEnabledPane;
import forteso.utils.DialogUtils;
import forteso.utils.IllegalCoordinateException;
import forteso.utils.LatLongCreator;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;

public class AddPOIDialogController implements Initializable {
    private static final URL LAYOUT = Objects.requireNonNull(
	    AddPOIDialogController.class.getResource("/layout_add_poi.fxml"),
	    () -> "Cannot find resource layout_add_poi");

    @FXML
    private TextField labelField;
    @FXML
    private RadioButton radioAddress;
    @FXML
    private RadioButton radioLatLong;
    @FXML
    private ListView<String> addrListView;
    @FXML
    private Node latLongPane;
    @FXML
    private TextField latField;
    @FXML
    private TextField lonField;
    private BooleanBinding validInputBinding;

    private SortedMap<String, HouseNumber> houseNumbers;
    private Map<HouseNumber, LatLong> coordinates;

    public AddPOIDialogController(Map<HouseNumber, LatLong> availableNumbers) {
	coordinates = Objects.requireNonNull(availableNumbers);
	houseNumbers = new TreeMap<String, HouseNumber>(
		String.CASE_INSENSITIVE_ORDER);
	availableNumbers.keySet().forEach(number -> {
	    String displayText = number.getStreetName() + ", "
		    + number.getNumber();
	    houseNumbers.put(displayText, number);
	});
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
	RadioEnabledPane pane = new RadioEnabledPane();
	pane.add(radioAddress, addrListView);
	pane.add(radioLatLong, latLongPane);

	addrListView.getItems().setAll(houseNumbers.keySet());
	addrListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

	validInputBinding = Bindings.createBooleanBinding(this::isInputValid,
		labelField.textProperty(), radioAddress.selectedProperty(),
		radioLatLong.selectedProperty(),
		addrListView.getSelectionModel().selectedItemProperty(),
		latField.textProperty(), lonField.textProperty());
    }

    private boolean isInputValid() {
	String label = labelField.getText().trim();
	if (label.isEmpty() || houseNumbers.containsKey(label)) {
	    return false;
	}
	if (radioAddress.isSelected()
		&& addrListView.getSelectionModel().getSelectedItem() == null) {
	    return false;
	}
	if (radioLatLong.isSelected()) {
	    try {
		double lat = Double.parseDouble(latField.getText());
		double lon = Double.parseDouble(lonField.getText());
		if (!Double.isFinite(lat) || !Double.isFinite(lon)) {
		    return false;
		}
	    } catch (NumberFormatException e) {
		return false;
	    }
	}
	return radioAddress.isSelected() ^ radioLatLong.isSelected();
    }

    private POI createPOI() throws IllegalCoordinateException {
	String label = labelField.getText().trim();
	LatLong coords = null;
	if (radioAddress.isSelected()) {
	    String selection = addrListView.getSelectionModel()
		    .getSelectedItem();
	    coords = coordinates.get(houseNumbers.get(selection));
	} else {
	    double lat = Double.parseDouble(latField.getText());
	    double lon = Double.parseDouble(lonField.getText());
	    coords = LatLongCreator.createChecked(lat, lon);
	}
	return new POI(coords, label);
    }

    public void showDialog(Consumer<POI> cons) {
	FXMLLoader loader = new FXMLLoader(LAYOUT);
	loader.setController(this);
	try {
	    Dialog<ButtonType> dialog = new Dialog<>();
	    Node content = loader.load();
	    dialog.getDialogPane().setContent(content);
	    ButtonType cancel = new ButtonType("Abbrechen",
		    ButtonData.CANCEL_CLOSE);
	    ButtonType ok = new ButtonType("Hinzufügen", ButtonData.OK_DONE);
	    dialog.getDialogPane().getButtonTypes().add(cancel);
	    dialog.getDialogPane().getButtonTypes().add(ok);
	    Node confirmNode = dialog.getDialogPane().lookupButton(ok);
	    confirmNode.disableProperty().bind(validInputBinding.not());
	    Runnable run = () -> {
		dialog.showAndWait().filter(ok::equals).ifPresent(t -> {
		    try {
			// get data
			POI createdPOI = createPOI();
			cons.accept(createdPOI);
		    } catch (IllegalCoordinateException e) {
			DialogUtils.showInfoDialog("Ungültige Koordinaten",
				"Der erzeugte Punkt enthält ungültige Koordinaten\n"
					+ "Wertebreich der Koordinaten ist [-90.0;+90.0]");
		    }
		});
	    };
	    Platform.runLater(run);
	} catch (IOException e) {
	    e.printStackTrace();
	    DialogUtils.showBlockingErrorDialog("Fehler beim Laden", e);
	}
    }
}
