/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.mapsforge.core.model.LatLong;

import forteso.trainer.POI;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;

public class CustomizeController extends TabController {
    private static final String LAYOUT = "/layout_customize.fxml";
    private static final String TITLE = "Anpassen";
    private static final String TOOLTIP = "Straßen und wichtige Punkte hinzufügen, editieren oder löschen";

    @FXML
    private ListView<POIProxy> poiListView;
    @FXML
    private Button addPOIButton;
    @FXML
    private Button removePOIButton;
    @FXML
    private Label infoPOILabel;

    private MultipleSelectionModel<POIProxy> selectionModel;

    public CustomizeController() {
	super(LAYOUT, TITLE, TOOLTIP);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	selectionModel = poiListView.getSelectionModel();
	selectionModel.setSelectionMode(SelectionMode.MULTIPLE);

	selectionModel.selectedItemProperty().addListener((obs, old, newV) -> {
	    StringBuilder b = new StringBuilder();
	    selectionModel.getSelectedItems()
		    .forEach(proxy -> b.append(proxy.showInfo()));
	    infoPOILabel.setText(b.toString());
	});
	poiListView.setCellFactory(this::createContextMenuFor);

	removePOIButton.setOnAction(this::removeSelection);

	addPOIButton.setOnAction(e -> {
	    AddPOIDialogController controller = new AddPOIDialogController(
		    builder.getHouseNumbers());
	    controller.showDialog(this::addPOI);
	});
    }

    private ListCell<POIProxy> createContextMenuFor(ListView<POIProxy> lv) {
	ListCell<POIProxy> cell = new ListCell<>();

	ContextMenu menu = new ContextMenu();

	MenuItem removeSingle = new MenuItem();

	removeSingle.textProperty()
		.bind(Bindings.format("%s entfernen", cell.itemProperty()));
	removeSingle.setOnAction(e -> removePOI(cell.itemProperty().get()));

	MenuItem removeSelected = new MenuItem();
	removeSelected.textProperty().bind(Bindings.createStringBinding(
		() -> String.format("Auswahl (%d) entfernen",
			lv.selectionModelProperty().get().getSelectedItems()
				.size()),
		lv.selectionModelProperty().get().selectedIndexProperty()));
	removeSelected.setOnAction(this::removeSelection);

	menu.getItems().addAll(removeSingle, removeSelected);
	cell.textProperty().bind(Bindings.createStringBinding(
		() -> cell.getItem() != null ? cell.getItem().toString() : "",
		cell.itemProperty()));
	cell.emptyProperty().addListener((obs, wasEmpty, isNowEmpty) -> {
	    if (isNowEmpty) {
		cell.setContextMenu(null);
	    } else {
		cell.setContextMenu(menu);
	    }
	});
	return cell;
    }

    private void addPOI(POI p) {
	builder.addPOI(p);
	update();
    }

    private void removeSelection(ActionEvent e) {
	selectionModel.getSelectedItems()
		.forEach(proxy -> builder.removePOI(proxy.getPOI()));
	selectionModel.clearSelection();
	update();
    }

    private void removePOI(POIProxy proxy) {
	builder.removePOI(proxy.getPOI());
	update();
    }

    private void updateList() {
	List<POIProxy> selected = selectionModel.getSelectedItems();
	List<POIProxy> proxies = builder.getPOIs().stream().map(POIProxy::new)
		.sorted().collect(Collectors.toList());
	poiListView.getItems().setAll(proxies);
	selected.forEach(selection -> selectionModel.select(selection));
	removePOIButton.setDisable(proxies.isEmpty());
    }

    @Override
    public boolean canContinue() {
	return true;
    }

    @Override
    public String computeMissingInputMessage() {
	return null;
    }

    @Override
    public void update() {
	super.update();
	updateList();
    }

    private static class POIProxy implements Comparable<POIProxy> {
	private final POI poi;

	public POIProxy(POI p) {
	    poi = Objects.requireNonNull(p);
	}

	public POI getPOI() {
	    return poi;
	}

	@Override
	public String toString() {
	    return poi.getLabel();
	}

	@Override
	public boolean equals(Object o) {
	    if (o == null || !(o instanceof POIProxy)) {
		return false;
	    }
	    return poi.getLabel().equals(((POIProxy) o).poi.getLabel());
	}

	@Override
	public int hashCode() {
	    return poi.getLabel().hashCode();
	}

	@Override
	public int compareTo(POIProxy o) {
	    return poi.getLabel().compareTo(o.poi.getLabel());
	}

	public String showInfo() {
	    LatLong ll = poi.getCoordinates();
	    return poi.getLabel() + " (" + ll.latitude + "°, " + ll.longitude
		    + "°)";
	}
    }
}
