/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import forteso.exporter.ExportFilterTask;
import forteso.exporter.ExportPOITask;
import forteso.exporter.ExportTaskRunner;
import forteso.exporter.JarUpdateTask;
import forteso.utils.DialogUtils;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser.ExtensionFilter;

public class ExportTabController extends TabController {
    private static final String LAYOUT = "/layout_export.fxml";
    private static final String TITLE = "Fertigstellen";
    private static final String TOOLTIP = "Die ausgewählten Kartendaten exportieren";
    private static final String NEXT_BUTTON_TEXT = "Fertig";

    private static final ExtensionFilter JAR_FILE = new ExtensionFilter(
	    "Ausführbare Java-Datei", "*.jar");

    @FXML
    private Label executablePath;
    @FXML
    private Button openExecutable;
    @FXML
    private CheckBox checkExportPOI;
    @FXML
    private TextField pathExportPOI;
    @FXML
    private Button openExportPOI;
    @FXML
    private CheckBox checkExportFilter;
    @FXML
    private TextField pathExportFilter;
    @FXML
    private Button openExportFilter;

    @FXML
    private Label progressLabel;
    @FXML
    private ProgressBar progressBar;

    private boolean isRunning;

    public ExportTabController() {
	super(LAYOUT, TITLE, TOOLTIP, NEXT_BUTTON_TEXT);
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
	FileSelectionController executable = new FileSelectionController(
		executablePath, openExecutable);
	executable.setFileChooser("Ausführbare Datei auswählen", file -> {
	    String path = file.getAbsolutePath();
	    executable.setText(path);
	    builder.setExecutablePath(file);
	    update();
	}, JAR_FILE);

	checkExportPOI.selectedProperty().addListener((obsV, old, newV) -> {
	    if (!newV) {
		pathExportPOI.setText("");
		builder.clearPOIExportPath();
	    }
	    update();
	});
	checkExportFilter.selectedProperty().addListener((obsV, old, newV) -> {
	    if (!newV) {
		pathExportFilter.setText("");
		builder.clearStreetFilterExportPath();
	    }
	    update();
	});

	FileSaveController poiExport = new FileSaveController(pathExportPOI,
		openExportPOI);
	poiExport.setFileChooser("Speicherort für Datei wählen", file -> {
	    String path = file.getAbsolutePath();
	    if (!path.endsWith(".osm")) {
		path += ".osm";
	    }
	    poiExport.setText(path);
	    builder.setPOIExportPath(new File(path));
	    update();
	}, OSM_FILTER);

	FileSaveController filterExport = new FileSaveController(
		pathExportFilter, openExportFilter);
	filterExport.setFileChooser("Speicherort für Datei wählen", file -> {
	    String path = file.getAbsolutePath();
	    if (!path.endsWith(".txt")) {
		path += ".txt";
	    }
	    filterExport.setText(path);
	    builder.setStreetFilterExportPath(new File(path));
	    update();
	}, STREET_FILTER_FILE);
    }

    @Override
    public boolean canContinue() {
	File executable = builder.getExecutablePath();
	return !isRunning && executable != null && executable.exists()
		&& executable.isFile();
    }

    @Override
    public String computeMissingInputMessage() {
	return "Ausführbare Java-Datei auswählen";
    }

    @Override
    public void onNextButtonClicked() {
	super.onNextButtonClicked();
	ExportTaskRunner runner = new ExportTaskRunner(new ProgressHandler());
	if (checkExportPOI.isSelected()) {
	    runner.add(new ExportPOITask(builder, getSelectedCharset()));
	}
	if (checkExportFilter.isSelected()) {
	    runner.add(new ExportFilterTask(builder, getSelectedCharset()));
	}
	runner.add(new JarUpdateTask(builder));
	runner.runNonBlocking();
    }

    private class ProgressHandler implements ProgressDisplay {

	@Override
	public DoubleProperty progress() {
	    return progressBar.progressProperty();
	}

	@Override
	public StringProperty info() {
	    return progressLabel.textProperty();
	}

	@Override
	public void onSuccess(String msg) {
	    DialogUtils.showInfoDialog("Fertig!", msg);
	}

	@Override
	public void onFailure(String msg, Throwable t) {
	    System.out.println(msg);
	    t.printStackTrace();
	    Platform.runLater(
		    () -> DialogUtils.showBlockingErrorDialog(msg, t));
	}

    }

}
