/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;

public abstract class FileChooserController {
    public static final String EMPTY_PATH_TEXT = "Keine Datei ausgewählt";

    private final TextContainer infoLabel;
    private final Button openFCButton;
    private final Optional<Button> resetButton;
    private String resetText;
    private Optional<Runnable> onReset;
    private FileChooser chooser;
    private Consumer<File> consumer;

    public FileChooserController(TextContainer label, Button openButton,
	    Optional<Button> reset) {
	infoLabel = Objects.requireNonNull(label);
	openFCButton = Objects.requireNonNull(openButton);
	resetButton = Objects.requireNonNull(reset);
	resetText = EMPTY_PATH_TEXT;
	onReset = Optional.empty();
	attachListener();
	setText(resetText);
    }

    protected abstract File performFileChooserAction(FileChooser c);

    private void attachListener() {
	resetButton.ifPresent(button -> button.setOnAction(this::reset));
	openFCButton.setOnAction(e -> {
	    reset(e);
	    File selectedFile = performFileChooserAction(chooser);
	    if (selectedFile != null) {
		consumer.accept(selectedFile);
	    }
	});
    }

    public void setResetBehaviour(String msg, Runnable listener) {
	resetText = Objects.requireNonNull(msg);
	onReset = Optional.ofNullable(listener);
	setText(resetText);
    }

    public void setText(String text) {
	setLabelText(text);
	resetButton.ifPresent(button -> button.setDisable(false));
    }

    public void setFileChooser(String title, Consumer<File> fileConsumer,
	    FileChooser.ExtensionFilter... filters) {
	chooser = new FileChooser();
	chooser.setTitle(title);
	chooser.getExtensionFilters().addAll(filters);
	consumer = Objects.requireNonNull(fileConsumer);
    }

    protected void reset(ActionEvent e) {
	reset();
	onReset.ifPresent(Runnable::run);
    }

    private void reset() {
	setLabelText(resetText);
	resetButton.ifPresent(button -> button.setDisable(true));
    }

    private void setLabelText(String s) {
	infoLabel.setText(s);
	infoLabel.setTooltip(new Tooltip(s));
    }

    protected interface TextContainer {
	void setText(String s);

	void setTooltip(Tooltip t);
    }

}