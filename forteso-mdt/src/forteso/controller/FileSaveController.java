/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.util.Objects;
import java.util.Optional;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;

public class FileSaveController extends FileChooserController {

    public FileSaveController(TextField field, Button openButton,
	    Optional<Button> reset) {
	super(new TextFieldContainer(field), openButton, reset);
	setResetBehaviour("", null);
    }

    public FileSaveController(TextField field, Button openButton) {
	this(field, openButton, Optional.empty());
    }

    @Override
    protected File performFileChooserAction(FileChooser c) {
	return c.showSaveDialog(null);
    }

    private static class TextFieldContainer implements TextContainer {
	private final TextField delegate;

	public TextFieldContainer(TextField f) {
	    delegate = Objects.requireNonNull(f);
	}

	@Override
	public void setText(String s) {
	    delegate.setText(s);
	}

	@Override
	public void setTooltip(Tooltip t) {
	    delegate.setTooltip(t);
	}

    }
}
