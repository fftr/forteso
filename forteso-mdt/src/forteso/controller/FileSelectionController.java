/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.util.Objects;
import java.util.Optional;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;

public class FileSelectionController extends FileChooserController {

    private FileSelectionController(Label pathLabel, Button openButton,
	    Optional<Button> reset) {
	super(new LabelTextContainer(pathLabel), openButton, reset);
    }

    public FileSelectionController(Label pathLabel, Button openButton,
	    Button resetButton) {
	this(pathLabel, openButton, Optional.of(resetButton));
    }

    public FileSelectionController(Label pathLabel, Button openButton) {
	this(pathLabel, openButton, Optional.empty());
    }

    @Override
    protected File performFileChooserAction(FileChooser chooser) {
	return chooser.showOpenDialog(null);
    }

    private static class LabelTextContainer implements TextContainer {
	private final Label delegate;

	public LabelTextContainer(Label l) {
	    delegate = Objects.requireNonNull(l);
	}

	@Override
	public void setText(String s) {
	    delegate.setText(s);
	}

	@Override
	public void setTooltip(Tooltip t) {
	    delegate.setTooltip(t);
	}

    }

}
