/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Objects;
import java.util.ResourceBundle;

import javax.xml.parsers.ParserConfigurationException;

import forteso.io.MapDataCorruptedException;
import forteso.utils.DialogUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class ImportTabController extends TabController {

    private static final String LAYOUT = "/layout_import.fxml";
    private static final String TITLE = "Importieren";
    private static final String TOOLTIP = "Vorhandene Straßen- und Punktedaten öffnen";

    @FXML
    private Button openPOI;
    @FXML
    private Button openWhitelist;
    @FXML
    private Button resetPOI;
    @FXML
    private Button resetFilter;
    @FXML
    private Button generate;
    @FXML
    private Label pathPOI;
    @FXML
    private Label pathWhitelist;
    @FXML
    private ListView<String> listPOI;
    @FXML
    private ListView<String> listWhitelist;

    private PreviewController poiPreview;
    private PreviewController whitelistPreview;

    public ImportTabController() {
	super(LAYOUT, TITLE, TOOLTIP);
    }

    private void openPOI(File f) {
	builder.clearPOIs();
	try {
	    URL selected = f.toURI().toURL();
	    builder.loadPOIs(selected);
	    poiPreview.update(selected.getPath(), builder.getPOILabels());
	    update();
	} catch (MapDataCorruptedException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Lesen der Datei. Die Datei scheint keine POI-Datei zu sein",
		    e);
	} catch (MalformedURLException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Auflösen des Pfads " + f.getAbsolutePath(), e);
	} catch (ParserConfigurationException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Schwerwiegender interner Fehler", e);
	} catch (IOException e) {
	    DialogUtils.showBlockingErrorDialog("Fehler beim Lesen der Datei "
		    + f.getAbsolutePath() + " Ein-/Ausgabe Fehler", e);
	}

    }

    private void openFilter(File f) {
	builder.clearFilter();
	try {
	    URL selected = f.toURI().toURL();
	    builder.loadFilter(selected, getSelectedCharset());
	    whitelistPreview.update(selected.getPath(), builder.getFilter());
	    update();
	} catch (MapDataCorruptedException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Lesen der Datei. Die Datei scheint nicht das richtige Format zu haben.\n"
			    + "(Mit newline oder carriage return getrennte Straßennamen)",
		    e);
	} catch (MalformedURLException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Auflösen des Pfads " + f.getAbsolutePath(), e);
	} catch (IOException e) {
	    DialogUtils.showBlockingErrorDialog("Fehler beim Lesen der Datei "
		    + f.getAbsolutePath() + ". Ein-/Ausgabe Fehler", e);
	}
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
	poiPreview = new PreviewController(pathPOI, openPOI, listPOI, resetPOI);
	poiPreview.setFileChooser("Datei mit wichtigen Punkten auswählen",
		this::openPOI, OSM_FILTER);
	poiPreview.setResetBehaviour(FileChooserController.EMPTY_PATH_TEXT,
		() -> builder.removePOIs(listPOI.getItems()));

	whitelistPreview = new PreviewController(pathWhitelist, openWhitelist,
		listWhitelist, resetFilter);
	whitelistPreview.setFileChooser("Datei mit Straßennamen auswählen",
		this::openFilter, STREET_FILTER_FILE);
	whitelistPreview.setResetBehaviour(
		FileChooserController.EMPTY_PATH_TEXT,
		() -> builder.removeFilter(listWhitelist.getItems()));

	generate.setOnAction(event -> openFCAndConsume("Kartendaten auswählen",
		OSM_FILTER, selected -> {
		    builder.clearFilter();
		    builder.clearPOIs();
		    try {
			builder.generatePOIsAndFilter(selected);
			poiPreview.generated(builder.getPOILabels());
			whitelistPreview.generated(builder.getFilter());
		    } catch (MapDataCorruptedException e) {
			DialogUtils.showBlockingErrorDialog(
				"Fehler beim Lesen der Datei", e);
		    } catch (ParserConfigurationException e) {
			DialogUtils.showBlockingErrorDialog(
				"Schwerwiegender interner Fehler ", e);
		    } catch (IOException e) {
			DialogUtils.showBlockingErrorDialog(
				"Fehler beim Lesen der Datei "
					+ selected.getPath()
					+ ". Ein-/Ausgabe Fehler",
				e);
		    }
		}));
    }

    @Override
    public boolean canContinue() {
	return true;// always because invalid paths are rejected
    }

    @Override
    public String computeMissingInputMessage() {
	// no need for a message
	return null;
    }

    private class PreviewController extends FileSelectionController {
	private static final String GENERATED_TEXT = "%d Einträge generiert";
	private static final String PATH_TEXT = "%s\n%d Einträge gefunden";

	private ListView<String> elements;

	public PreviewController(Label pathLabel, Button openFCButton,
		ListView<String> listView, Button reset) {
	    super(pathLabel, openFCButton, reset);
	    elements = Objects.requireNonNull(listView);
	}

	public void update(String filePath, Collection<String> elementData) {
	    setText(String.format(PATH_TEXT, filePath, elementData.size()));
	    elements.getItems().clear();
	    elements.getItems().addAll(elementData);
	}

	public void generated(Collection<String> elementData) {
	    setText(String.format(GENERATED_TEXT, elementData.size()));
	    elements.getItems().clear();
	    elements.getItems().addAll(elementData);
	}

	@Override
	protected void reset(ActionEvent e) {
	    super.reset(e);
	    elements.getItems().clear();
	}
    }
}
