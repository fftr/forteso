/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.mapsforge.core.model.LatLong;

import forteso.io.MapDataCorruptedException;
import forteso.io.MapDataIO;
import forteso.io.parserextensions.ParserExtensionBundle;
import forteso.io.parserextensions.ParserModule;
import forteso.trainer.HouseNumber;
import forteso.trainer.Street;
import forteso.utils.DialogUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class MapDataController extends TabController {
    private static final String LAYOUT = "/layout_mapdata.fxml";
    private static final String TITLE = "Kartendaten";
    private static final String TOOLTIP = "Kartendaten auswählen";
    private static final String GRAPHICAL_TEXT = "Datei (%s) ausgewählt";
    private static final String LOCATION_TEXT = "Datei (%s) ausgewählt. %d Straßen gefunden";

    @FXML
    private Button openGraphicalMap;
    @FXML
    private Button openLocationData;

    @FXML
    private Label pathGraphicalMap;
    @FXML
    private Label pathLocationData;

    private Set<Street> addedStreets;
    private FileSelectionController graphicalMapSelection;
    private FileSelectionController locationMapSelection;

    private static final Comparator<Street> comp = (s1, s2) -> s1
	    .getStreetName().compareTo(s2.getStreetName());

    public MapDataController() {
	super(LAYOUT, TITLE, TOOLTIP);
	addedStreets = new TreeSet<>(comp);
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
	graphicalMapSelection = new FileSelectionController(pathGraphicalMap,
		openGraphicalMap);
	graphicalMapSelection.setFileChooser(
		"Karte für grapische Darstellung auswählen",
		this::openGraphicalMap, OSM_FILTER);

	locationMapSelection = new FileSelectionController(pathLocationData,
		openLocationData);
	locationMapSelection.setFileChooser(
		"Karte mit Trainingsdaten auswählen", this::openLocationData,
		OSM_FILTER);

    }

    private void openGraphicalMap(File f) {
	try {
	    URL selected = f.toURI().toURL();
	    builder.setGraphicalMap(selected);
	    graphicalMapSelection
		    .setText(String.format(GRAPHICAL_TEXT, f.getName()));
	} catch (MalformedURLException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Auflösen des Pfads " + f.getAbsolutePath(), e);
	}
	update();
    }

    private void openLocationData(File f) {
	try {
	    URL selected = f.toURI().toURL();
	    ParserExtensionBundle bundle = ParserModule.createConfiguration(
		    ParserModule.HOUSE_NUMBERS, ParserModule.STREETS);
	    MapDataIO.parse(selected, bundle);
	    addedStreets.clear();
	    Collection<Street> streets = bundle
		    .getParsedData(ParserModule.STREETS);
	    Map<HouseNumber, LatLong> housenrs = bundle
		    .getParsedData(ParserModule.HOUSE_NUMBERS);
	    addedStreets.addAll(streets);
	    locationMapSelection.setText(
		    String.format(LOCATION_TEXT, f.getName(), streets.size()));
	    builder.addStreets(streets);
	    builder.setLoadedHouseNumbers(housenrs);
	} catch (MalformedURLException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Auflösen des Pfads " + f.getAbsolutePath(), e);
	} catch (ParserConfigurationException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Schwerwiegender interner Fehler", e);
	} catch (MapDataCorruptedException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Lesen der Datei. Ungültiges Format "
			    + f.getAbsolutePath(),
		    e.getCause());
	} catch (IOException e) {
	    DialogUtils.showBlockingErrorDialog(
		    "Fehler beim Lesen der Datei. Ein-/Ausgabe Fehler "
			    + f.getAbsolutePath(),
		    e.getCause());
	}
	update();
    }

    @Override
    public boolean canContinue() {
	return hasGraphicalMap() && !addedStreets.isEmpty();
    }

    private boolean hasGraphicalMap() {
	return builder.getGraphicalMap() != null
		&& new File(builder.getGraphicalMap().getFile()).exists();
    }

    @Override
    public String computeMissingInputMessage() {
	if (!hasGraphicalMap()) {
	    return "Kartendaten für den Bildausschnitt auswählen";
	} else if (addedStreets.isEmpty()) {
	    return "Kartendaten als Trainingsdaten wählen";
	}
	return "";
    }

}
