/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;

public interface ProgressDisplay {

    DoubleProperty progress();

    StringProperty info();

    void onSuccess(String msg);

    void onFailure(String msg, Throwable t);

}
