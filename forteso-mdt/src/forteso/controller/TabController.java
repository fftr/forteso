/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

import forteso.exporter.ExportData;
import forteso.utils.DialogUtils;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.stage.FileChooser;

public abstract class TabController implements Initializable {

    protected static final FileChooser.ExtensionFilter OSM_FILTER = new FileChooser.ExtensionFilter(
	    "Open Street Map Dateien", "*.osm");
    protected static final FileChooser.ExtensionFilter STREET_FILTER_FILE = new FileChooser.ExtensionFilter(
	    ".TXT", "*.txt");
    private static final String DEFAULT_NEXT_BUTTON_TEXT = "Weiter";
    private WindowController superController;
    private Supplier<Charset> selectedCharset;
    private final URL layout;
    private final String tooltipText;
    private final String tabTitle;
    private final String nextButtonText;
    protected ExportData builder;

    protected TabController(String layout, String title, String tooltip,
	    String nextText) {
	this.layout = Objects.requireNonNull(
		TabController.class.getResource(layout),
		() -> "Cannot find resource " + layout);
	tooltipText = Objects.requireNonNull(tooltip,
		() -> "Tooltip may not be null");
	tabTitle = Objects.requireNonNull(title, () -> "Title may not be null");
	nextButtonText = Objects.requireNonNull(nextText);
    }

    protected TabController(String layout, String title, String tooltip) {
	this(layout, title, tooltip, DEFAULT_NEXT_BUTTON_TEXT);
    }

    Tab createTab() throws IOException {
	FXMLLoader loader = new FXMLLoader(layout);
	loader.setController(this);
	Node tabContent = loader.load();
	Tab tab = new Tab(tabTitle, tabContent);
	tab.setClosable(false);
	tab.setTooltip(new Tooltip(tooltipText));
	return tab;
    }

    String getNextButtonText() {
	return nextButtonText;
    }

    void setData(ExportData b) {
	builder = Objects.requireNonNull(b, () -> "Data may not be null");
    }

    void setControls(WindowController superControl, Supplier<Charset> charset) {
	superController = Objects.requireNonNull(superControl);
	selectedCharset = Objects.requireNonNull(charset,
		() -> "Charset may not be null");
    }

    public abstract boolean canContinue();

    public abstract String computeMissingInputMessage();

    public void onNextButtonClicked() {

    }

    public void onTabShown() {
	update();
    }

    protected void update() {
	superController.updateTab(this);
    }

    protected void openFCAndConsume(String fcTitle,
	    FileChooser.ExtensionFilter filter,
	    Consumer<URL> selectedFileConsumer) {
	FileChooser chooser = new FileChooser();
	chooser.setTitle(fcTitle);
	chooser.getExtensionFilters().add(filter);
	File selected = chooser.showOpenDialog(null);
	if (selected != null) {
	    if (selected.isDirectory() || !selected.exists()) {
		DialogUtils.showInfoDialog("Ungültige Auswahl",
			"Die ausgewählte Datei " + selected.getAbsolutePath()
				+ " scheint keine Datei zu sein oder nicht zu existieren");
	    } else {
		try {
		    selectedFileConsumer.accept(selected.toURI().toURL());
		} catch (MalformedURLException e) {
		    DialogUtils.showBlockingErrorDialog(
			    "Fehler beim Auflösen des Dateipfads "
				    + selected.getAbsolutePath(),
			    e);
		}
	    }
	}
    }

    protected Charset getSelectedCharset() {
	return selectedCharset.get();
    }
}
