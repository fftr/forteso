/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.controller;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.ResourceBundle;

import forteso.exporter.ExportData;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class WindowController implements Initializable {
    private static final TabController[] TABS = { new ImportTabController(),
	    new MapDataController(), new CustomizeController(),
	    new ExportTabController() };

    @FXML
    private TabPane tabContainer;
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Label info;
    @FXML
    private ComboBox<Charset> encodingCombobox;

    private int currentTab;

    private ExportData data;

    public WindowController(ExportData b) {
	data = Objects.requireNonNull(b, () -> "Data may not be null");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	backButton.setOnAction(this::back);
	nextButton.setOnAction(this::next);
	encodingCombobox.getItems()
		.addAll(Charset.availableCharsets().values());
	// select default charset
	encodingCombobox.getSelectionModel().select(Charset.defaultCharset());

	for (TabController controller : TABS) {
	    try {
		Tab tab = controller.createTab();
		tabContainer.getTabs().add(tab);
		controller.setControls(this, this::getSelectedCharset);
		controller.setData(data);
	    } catch (IOException e) {
		throw new RuntimeException(e);
	    }
	}
	currentTab = 0;
	selectTab(currentTab);
    }

    private void selectTab(int index) {
	tabContainer.getSelectionModel().select(index);
	currentTab = index;
	int i = 0;
	for (Tab tab : tabContainer.getTabs()) {
	    tab.setDisable(i != index);
	    i++;
	}
	TABS[currentTab].onTabShown();
	nextButton.setText(TABS[currentTab].getNextButtonText());
	updateTab(TABS[currentTab]);
    }

    private void back(ActionEvent e) {
	selectTab(currentTab - 1);
    }

    private void next(ActionEvent e) {
	TABS[currentTab].onNextButtonClicked();
	if (currentTab + 1 < TABS.length) {
	    selectTab(currentTab + 1);
	}
    }

    private Charset getSelectedCharset() {
	return encodingCombobox.getSelectionModel().getSelectedItem();
    }

    public void updateTab(TabController c) {
	boolean enable = c.canContinue();
	String infoMsg = enable ? "" : c.computeMissingInputMessage();
	info.setText(infoMsg);
	nextButton.setDisable(!enable);
	backButton.setDisable(currentTab <= 0);
    }

}
