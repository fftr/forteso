/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.ParserConfigurationException;

import org.mapsforge.core.model.LatLong;

import forteso.io.MapDataCorruptedException;
import forteso.io.MapDataIO;
import forteso.io.Whitelist;
import forteso.io.parserextensions.ParserExtensionBundle;
import forteso.io.parserextensions.ParserModule;
import forteso.trainer.HouseNumber;
import forteso.trainer.POI;
import forteso.trainer.Street;

public class ExportData {

    private Set<String> streetFilter;
    private Map<String, POI> pois;
    private Map<String, Street> loadedStreets;
    private Map<HouseNumber, LatLong> loadedHouseNumbers;
    private URL graphicMapURL;

    private File executable;
    private Optional<File> poiExportPath;
    private Optional<File> filterExportPath;

    public ExportData() {
	streetFilter = new TreeSet<>();
	pois = new TreeMap<>();
	loadedStreets = new TreeMap<>();
	loadedHouseNumbers = Collections.emptyMap();
	poiExportPath = Optional.empty();
	filterExportPath = Optional.empty();
    }

    public void setExecutablePath(File f) {
	executable = Objects.requireNonNull(f);
    }

    public void setPOIExportPath(File f) {
	poiExportPath = Optional.of(f);
    }

    public void clearPOIExportPath() {
	poiExportPath = Optional.empty();
    }

    public void setStreetFilterExportPath(File f) {
	filterExportPath = Optional.of(f);
    }

    public void clearStreetFilterExportPath() {
	filterExportPath = Optional.empty();
    }

    public void clearPOIs() {
	pois.clear();
    }

    public void clearFilter() {
	streetFilter.clear();
    }

    public void clearStreets() {
	loadedStreets.clear();
    }

    public void loadFilter(URL url, Charset charset)
	    throws MapDataCorruptedException, IOException {
	Whitelist wl = MapDataIO.parseWhitelist(url, charset);
	streetFilter.addAll(wl.listedValues());
    }

    public void loadPOIs(URL url) throws MapDataCorruptedException,
	    ParserConfigurationException, IOException {
	ParserExtensionBundle bundle = ParserModule
		.createConfiguration(ParserModule.POIS);
	MapDataIO.parse(url, bundle);
	addPOIs(bundle.getParsedData(ParserModule.POIS));
    }

    public void addPOIs(Collection<POI> coll) {
	coll.forEach(this::addPOI);
    }

    public void addStreets(Collection<Street> coll) {
	coll.forEach(
		street -> loadedStreets.put(street.getStreetName(), street));
    }

    public void setLoadedHouseNumbers(Map<HouseNumber, LatLong> map) {
	loadedHouseNumbers = Objects.requireNonNull(map);
    }

    public void removePOI(POI p) {
	pois.remove(p.getLabel());
    }

    public void generatePOIsAndFilter(URL url) throws MapDataCorruptedException,
	    ParserConfigurationException, IOException {
	ParserExtensionBundle bundle = ParserModule
		.createConfiguration(ParserModule.STREETS, ParserModule.POIS);
	MapDataIO.parse(url, bundle);
	Collection<POI> pois = bundle.getParsedData(ParserModule.POIS);
	Collection<Street> streets = bundle.getParsedData(ParserModule.STREETS);
	addPOIs(pois);
	streets.forEach(street -> streetFilter.add(street.getStreetName()));

    }

    public Set<String> getPOILabels() {
	return Collections.unmodifiableSet(pois.keySet());
    }

    public Collection<POI> getPOIs() {
	return Collections.unmodifiableCollection(pois.values());
    }

    public Set<String> getFilter() {
	return Collections.unmodifiableSet(streetFilter);
    }

    public void removePOIs(Collection<String> names) {
	names.forEach(pois::remove);
    }

    public void removeFilter(Collection<String> names) {
	streetFilter.removeAll(names);
    }

    public void setGraphicalMap(URL url) {
	graphicMapURL = Objects.requireNonNull(url, () -> "May not be null");
    }

    public URL getGraphicalMap() {
	return graphicMapURL;
    }

    public Map<HouseNumber, LatLong> getHouseNumbers() {
	return Collections.unmodifiableMap(loadedHouseNumbers);
    }

    public void addPOI(POI poi) {
	pois.put(poi.getLabel(), poi);
    }

    public File getExecutablePath() {
	return executable;
    }

    public Optional<File> getPOIExportPath() {
	return poiExportPath;
    }

    public Optional<File> getFilterExportPath() {
	return filterExportPath;
    }

    public Collection<Street> getStreets() {
	return loadedStreets.values();
    }
}
