/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

public class ExportException extends Exception {

    private static final long serialVersionUID = 2841262920054830053L;

    public ExportException(String msg, Throwable t) {
	super(msg, t);
    }

    public ExportException(String msg) {
	super(msg);
    }
}
