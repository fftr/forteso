/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;

import forteso.io.MapDataIO;

public class ExportFilterTask extends ExportTask {

    private Charset charset;

    public ExportFilterTask(ExportData d, Charset c) {
	super("Speichere Straßenfilter", d);
	charset = Objects.requireNonNull(c);
    }

    @Override
    public int estimateTicks() throws ExportException {
	return 2;
    }

    @Override
    public void performTask() throws ExportException {
	tick("Schreibe Straßennamen...");
	File destination = data.getFilterExportPath()
		.orElseThrow(() -> new ExportException(
			"BUG: Filter export task added without path"));
	try {
	    MapDataIO.writeStreetnameList(destination, charset,
		    data.getFilter());
	} catch (IOException e) {
	    throw new ExportException(getTitle(), e);
	}
	tick("Schreibe Straßennamen... Fertig");
    }

}
