/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Objects;

import forteso.io.MapDataIO;
import forteso.io.OsmDataWriter;
import forteso.io.OsmDocumentBuilder;
import forteso.trainer.POI;

public class ExportPOITask extends ExportTask {

    private Charset charset;

    public ExportPOITask(ExportData d, Charset c) {
	super("Speichere wichtige Punkte", d);
	charset = Objects.requireNonNull(c);
    }

    @Override
    public int estimateTicks() throws ExportException {
	return 2;
    }

    @Override
    public void performTask() throws ExportException {
	tick("Schreibe wichtige Punkte...");
	try {
	    File destination = data.getPOIExportPath()
		    .orElseThrow(() -> new ExportException(
			    "BUG: Task was added to export POIs, but no destination path was supplied"));
	    Collection<POI> pois = data.getPOIs();
	    OsmDataWriter writer = MapDataIO.getWriter();
	    OsmDocumentBuilder document = writer.startDocument();
	    document.addAllPOIs(pois);
	    try (FileOutputStream fos = new FileOutputStream(destination)) {
		// try-with-resources
		writer.writeDocument(document, fos, charset);
	    }

	} catch (IOException e) {
	    throw new ExportException(getTitle(), e);
	} catch (Throwable t) {
	    throw new ExportException(getTitle(), t);
	}
	tick("Schreibe wichtige Punkte... Fertig");
    }

}
