/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.util.Objects;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public abstract class ExportTask {

    protected ExportData data;
    private String title;
    private IntegerProperty ticks;
    private StringProperty tickDescription;

    protected ExportTask(String t, ExportData d) {
	ticks = new SimpleIntegerProperty(0);
	tickDescription = new SimpleStringProperty("");
	title = Objects.requireNonNull(t);
	data = Objects.requireNonNull(d);
    }

    public final String getTitle() {
	return title;
    }

    public final IntegerProperty ticks() {
	return ticks;
    }

    public final StringProperty description() {
	return tickDescription;
    }

    protected void tick(String msg) {
	ticks.set(ticks.get() + 1);
	tickDescription.set(msg);
    }

    public abstract int estimateTicks() throws ExportException;

    public abstract void performTask() throws ExportException;

}
