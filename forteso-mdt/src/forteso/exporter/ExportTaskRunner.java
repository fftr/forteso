/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import forteso.controller.ProgressDisplay;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.concurrent.Task;

public class ExportTaskRunner extends Task<Void> {
    private static final String BEGIN_MSG = "Schätze Arbeitsschritte ab";
    private static final String RUNNING_MESSAGE = "Schritt %d/%d: %s\n%s";
    private static final String END_MSG = "%d Schritte beendet";

    private final List<ExportTask> tasks;
    private final ProgressDisplay display;

    public ExportTaskRunner(ProgressDisplay d) {
	display = Objects.requireNonNull(d);
	tasks = new LinkedList<>();
    }

    public void add(ExportTask t) {
	tasks.add(t);
    }

    @Override
    protected Void call() throws Exception {
	// setup progress
	updateMessage(BEGIN_MSG);
	// estimate ticks
	int sum = 0;
	List<Integer> estimatedTicks = new ArrayList<>(tasks.size());
	for (ExportTask task : tasks) {
	    try {
		int ticks = task.estimateTicks();
		sum += ticks;
		estimatedTicks.add(ticks);
	    } catch (ExportException e) {
		display.onFailure(e.getMessage(), e);
		return null;
	    }
	}

	// setup progress bar
	updateProgress(0, sum);

	// begin
	int current = 1;
	final int size = tasks.size();
	final int s = sum;
	Iterator<Integer> it = estimatedTicks.iterator();
	int sumTicks = 0;
	for (ExportTask task : tasks) {
	    final int sumBisher = sumTicks;
	    task.ticks().addListener((obsV, old,
		    newV) -> updateProgress(sumBisher + newV.intValue(), s));

	    final int index = current;
	    StringBinding infoBinding = Bindings.createStringBinding(() -> {
		return String.format(RUNNING_MESSAGE, index, size,
			task.getTitle(), task.description().get());
	    }, task.description());
	    infoBinding.addListener((obsV, old, newV) -> updateMessage(newV));

	    try {
		task.performTask();
	    } catch (Throwable e) {
		display.onFailure(e.getMessage(), e);
		return null;
	    }
	    current++;
	    sumTicks += it.next();
	}
	return null;
    }

    @Override
    protected void updateProgress(long l, long a) {
	super.updateProgress(l, a);
	// System.out.println("Progress: " + l + "/" + a);
    }

    public void runNonBlocking() {
	ExecutorService exec = Executors.newSingleThreadExecutor();
	setOnSucceeded(
		e -> display.onSuccess(String.format(END_MSG, tasks.size())));
	display.info().bind(messageProperty());
	display.progress().bind(progressProperty());
	exec.submit(this);
	exec.shutdown();
    }

}
