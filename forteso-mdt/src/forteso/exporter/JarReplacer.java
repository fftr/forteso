/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import forteso.utils.IOStreamUtils;

public class JarReplacer {

    private Map<Predicate<String>, CheckedSupplier<InputStream>> replacements;

    public JarReplacer() {
	replacements = new HashMap<>();
    }

    public void addReplacementRule(Predicate<String> pred,
	    CheckedSupplier<InputStream> is) {
	if (replacements.put(pred, is) != null) {
	    throw new IllegalArgumentException("Duplicate predicate!");
	}
    }

    public void replace(JarInputStream jarFile, File out) throws IOException {
	Manifest newManifest = new Manifest();
	Manifest old = jarFile.getManifest();
	copyManifest(old, newManifest);
	try (JarOutputStream jos = new JarOutputStream(
		new FileOutputStream(out), newManifest)) {
	    JarEntry entry;
	    while ((entry = jarFile.getNextJarEntry()) != null) {
		InputStream is = handleEntry(jarFile, entry);
		JarEntry outEntry = null;
		if (is != null) {
		    // replacement
		    outEntry = new JarEntry(entry.getName());
		} else {
		    // no replacement
		    outEntry = entry;
		    is = jarFile;
		}
		try {
		    jos.putNextEntry(outEntry);
		    IOStreamUtils.transfer(is, jos);
		    jos.closeEntry();
		} finally {
		    // close is only if it was a replacement stream
		    if (is != null && is != jarFile) {
			is.close();
		    }
		}
	    }

	}
    }

    private void copyManifest(Manifest from, Manifest to) {
	for (Entry<Object, Object> attrEntry : from.getMainAttributes()
		.entrySet()) {
	    to.getMainAttributes().put(attrEntry.getKey(),
		    attrEntry.getValue());
	}
    }

    private InputStream handleEntry(JarInputStream jis, JarEntry e)
	    throws IOException {
	for (Predicate<String> pred : replacements.keySet()) {
	    if (pred.test(e.getName())) {
		return replacements.get(pred).get();
	    }
	}
	return null;
    }

    public interface CheckedSupplier<T> {
	T get() throws IOException;
    }
}
