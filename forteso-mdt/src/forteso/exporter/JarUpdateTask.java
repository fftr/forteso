/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.exporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.mapsforge.core.model.BoundingBox;

import forteso.io.MapDataCorruptedException;
import forteso.io.MapDataIO;
import forteso.io.parserextensions.ParserExtensionBundle;
import forteso.io.parserextensions.ParserModule;
import forteso.trainer.Street;
import forteso.trainer.TrainingArea;
import forteso.utils.TempFile;

public class JarUpdateTask extends ExportTask {

    public JarUpdateTask(ExportData d) {
	super("Aktualisiere Client-Version", d);
    }

    @Override
    public int estimateTicks() throws ExportException {
	return 5;
    }

    @Override
    public void performTask() throws ExportException {
	tick("Filtere Straßen");
	final Set<String> filter = data.getFilter();
	Set<Street> streets = data.getStreets().stream()
		.filter(s -> filter.contains(s.getStreetName()))
		.collect(Collectors.toSet());
	tick("Berechne Sichtfeld");
	try (TempFile tmp = TempFile.createTempFile("forteso-location",
		".osm.tmp")) {
	    ParserExtensionBundle bundle = ParserModule
		    .createConfiguration(ParserModule.BOUNDING_BOX);
	    MapDataIO.parse(data.getGraphicalMap(), bundle);
	    Optional<BoundingBox> bbOpt = bundle
		    .getParsedData(ParserModule.BOUNDING_BOX);
	    BoundingBox bb = bbOpt.orElseThrow(() -> new ExportException(
		    "Fehlerhafte Kartendaten in " + data.getGraphicalMap()
			    + ". Bounding Box Definition nicht gefunden"));

	    tick("Erzeuge Kartendaten");
	    TrainingArea locationData = new TrainingArea(bb, Optional.empty());
	    streets.forEach(locationData::addStreet);
	    data.getPOIs().forEach(locationData::addPOI);

	    System.out.println("Temporary file " + tmp.getAbsolutePath());
	    MapDataIO.writeOptimizedMap(tmp, locationData,
		    MapDataIO.DEFAULT_CHARSET);
	    tick("Generiere ausführbare Datei");
	    JarReplacer repl = createReplacer(tmp);
	    try (JarInputStream jis = new JarInputStream(
		    new FileInputStream(data.getExecutablePath()))) {
		File result = new File(
			data.getExecutablePath().getAbsolutePath() + ".gen");
		repl.replace(jis, result);
	    }
	    tick("Fertig");
	} catch (ParserConfigurationException | MapDataCorruptedException
		| IOException e) {
	    throw new ExportException(getTitle(), e);
	}
    }

    private JarReplacer createReplacer(File locTmp) {
	JarReplacer repl = new JarReplacer();
	repl.addReplacementRule((path) -> path.contains("locations.osm"),
		() -> new FileInputStream(locTmp));
	return repl;
    }
}
