/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.main;

import java.net.URL;
import java.util.Objects;

import forteso.controller.WindowController;
import forteso.exporter.ExportData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MasterApplication extends Application {

    public static void main(String[] args) {
	launch(MasterApplication.class, args);
    }

    @Override
    public void start(Stage stage) throws Exception {
	URL layout = Objects.requireNonNull(
		getClass().getResource("/layout.fxml"),
		() -> "Layout not found");
	FXMLLoader loader = new FXMLLoader(layout);
	loader.setController(new WindowController(new ExportData()));
	Parent parent = loader.load();
	stage.setScene(new Scene(parent));
	stage.show();
    }

}
