/*******************************************************************************
 * Copyright 2017 Erik Pohle, Arian Mehmanesh
 *
 * This file is part of Forteso.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 License.
 * To view a copy of the license, visit https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 * LPGL licensed Library:
 *   Forteso common: Copyright 2017 Erik Pohle, Arian Mehmanesh
 *******************************************************************************/
package forteso.uiutils;

import javafx.scene.Node;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class RadioEnabledPane {

    private ToggleGroup toggleGroup;

    public RadioEnabledPane() {
	toggleGroup = new ToggleGroup();
    }

    public void add(RadioButton radioButton, Node panel) {
	radioButton.setToggleGroup(toggleGroup);
	panel.disableProperty().bind(radioButton.selectedProperty().not());
    }

}
