import { Component } from '@angular/core';
import * as L from 'leaflet';
import { LatLngBounds } from 'leaflet-map';
import LatLonCoordinate from '../../model/geocoding/LatLonCoordinate';
import BoundingBox from '../../model/geocoding/BoundingBox';

import { StateProvider } from '../../providers/state-provider';
import {ErrorHandler} from '../../providers/error-handler';
/**
 * Generated class for the MapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'map-component',
  templateUrl: 'map-component.html'
})
export class MapComponent {
  private static readonly URL_ERROR = 'Eine Tile-Server URL ist ungültig oder kann nicht gelesen werden';
  private map: any;
  private labelsVisible: boolean;
  private layer: any;
  private greenIcon: any;
  private blueIcon: any;
  private homeIcon: any;
  private greenMarker: any;
  private blueMarker: any;
  private homeMarker: any;
  private streetPolyline: Array<any>;
  private closestPointPolyline: any;

  private clickEventHandler: (LatLonCoordinate) => void;

  private url: string;
  private urlNoLabel: string;


  constructor(private state: StateProvider, private error: ErrorHandler) {
    this.greenIcon = L.icon({
      iconUrl: 'assets/icon/green.png',
      shadowUrl: 'assets/icon/green.png',

      iconSize: [32, 37], // size of the icon
      shadowSize: [32, 37], // size of the shadow
      iconAnchor: [16, 32], // point of the icon which will correspond to marker's location
      shadowAnchor: [16, 32],  // the same for the shadow
      popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    this.blueIcon = L.icon({
      iconUrl: 'assets/icon/blue.png',
      shadowUrl: 'assets/icon/blue.png',

      iconSize: [32, 37], // size of the icon
      shadowSize: [32, 37], // size of the shadow
      iconAnchor: [16, 32], // point of the icon which will correspond to marker's location
      shadowAnchor: [16, 32],  // the same for the shadow
      popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    })

    this.homeIcon = L.icon({
      iconUrl: 'assets/icon/home.png',
      shadowUrl: 'assets/icon/home.png',

      iconSize: [32, 37], // size of the icon
      shadowSize: [32, 37], // size of the shadow
      iconAnchor: [16, 32], // point of the icon which will correspond to marker's location
      shadowAnchor: [16, 32],  // the same for the shadow
      popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    this.greenMarker = null;
    this.blueMarker = null;
    this.homeMarker = null;
    this.streetPolyline = null;
    this.closestPointPolyline = null;
  }

  ngOnInit(): void {

  }

  public initMap(bb: BoundingBox) {
    let bounds: LatLngBounds = L.latLngBounds(bb.min.toLeafletLatLng(), bb.max.toLeafletLatLng());
    this.map = L.map('map')
      .fitBounds(bounds);

    this.state.tileServerURL.subscribe(url => {
      this.url = url;
      this.layer = L.tileLayer(url, {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://wmflabs.org">Wikimedia</a>',
        maxZoom: 18
      });
      this.layer.addTo(this.map);
      this.labelsVisible = true;
      //subscribe to both urls and change the url if the labels are (not) visible
      //skipping the initial value
      this.state.tileServerURL.skip(1).subscribe(url => {
        this.url = url;
        if (this.labelsVisible) {
          this.layer.setUrl(url, false);
        }
      }, err => this.error.reportError(err, MapComponent.URL_ERROR));
      this.state.tileServerNoLabelURL.subscribe(url => {
        this.urlNoLabel = url;
        if (!this.labelsVisible) {
          this.layer.setUrl(url, false);
        }
      }, err => this.error.reportError(err, MapComponent.URL_ERROR));
    }).unsubscribe();
  }

  public setClickHandler(handler: (LatLonCoordinate) => void): void {
    this.clickEventHandler = handler;
    if (this.clickEventHandler) {
      this.map.on('click', ev => {
        let latlng = ev.latlng;
        this.clickEventHandler(new LatLonCoordinate(latlng.lat, latlng.lng));
      });
    } else {
      this.map.on('click', null);
    }
  }

  public placeHomeMarker(latlng: LatLonCoordinate): void { //example: placeHomeMarker([51.505, -0.09]);
    this.homeMarker = L.marker(latlng.toLeafletLatLng(), { icon: this.homeIcon });
    this.map.addLayer(this.homeMarker);
  }

  public placeGreenMarker(latlng: LatLonCoordinate): void {
    this.greenMarker = L.marker(latlng.toLeafletLatLng(), { icon: this.greenIcon });
    this.map.addLayer(this.greenMarker);
  }

  public placeBlueMarker(latlng: LatLonCoordinate): void {
    this.blueMarker = L.marker(latlng.toLeafletLatLng(), { icon: this.blueIcon });
    this.map.addLayer(this.blueMarker);
  }

  public placeStreetPolyline(latlngs: Array<Array<LatLonCoordinate>>, lineColor: string, lineWeight: number, lineTransparency: number): void { // example usage: placeStreetPolyline(..., 'green', 10, 0.5);
    this.streetPolyline = latlngs.map(llarray => L.polyline(llarray.map(llc => llc.toLeafletLatLng()), { color: lineColor, weight: lineWeight, opacity: lineTransparency }));
    this.streetPolyline.forEach(line => this.map.addLayer(line));
  }

  public removeGreenMarker(): void {
    if (this.greenMarker) {
      this.map.removeLayer(this.greenMarker);
      this.greenMarker = null;
    }
  }

  public removeBlueMarker(): void {
    if (this.blueMarker) {
      this.map.removeLayer(this.blueMarker);
      this.blueMarker = null;
    }
  }

  public removeHomeMarker(): void {
    if (this.homeMarker) {
      this.map.removeLayer(this.homeMarker);
      this.homeMarker = null;
    }
  }

  public removeStreetPolyline(): void {
    if (this.streetPolyline) {
      this.streetPolyline.forEach(line => this.map.removeLayer(line));
      this.streetPolyline = null;
    }
  }

  public setLabelVisible(visible: boolean): void {
    if (visible) {
      this.layer.setUrl(this.url, false);
    } else {
      this.layer.setUrl(this.urlNoLabel, false);
    }
    this.labelsVisible = visible;
  }

  public placeClosestPointPolyline(from: LatLonCoordinate, to: LatLonCoordinate, lineColor: string, lineWeight: number, lineTransparency: number): void {
    this.closestPointPolyline = L.polyline([from.toLeafletLatLng(), to.toLeafletLatLng()], { color: lineColor, weight: lineWeight, opacity: lineTransparency });
    this.map.addLayer(this.closestPointPolyline);
  }

  public removeClosestPointPolyline(): void {
    if (this.closestPointPolyline) {
      this.map.removeLayer(this.closestPointPolyline);
      this.closestPointPolyline = null;
    }
  }


}
