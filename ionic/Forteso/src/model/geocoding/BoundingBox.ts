import LatLonCoordinate from './LatLonCoordinate';

export default class BoundingBox {
  constructor(public readonly min: LatLonCoordinate, public readonly max: LatLonCoordinate) {
    
  }
}
