import { parseString } from 'xml2js';
import BoundingBox from './BoundingBox';
import { Street, StreetSegment } from './Street';
import LatLonCoordinate from './LatLonCoordinate';
import { TrainingArea } from './TrainingArea';
import {POI} from './poi';

export default class FortesoOSMParser {

  //FORMAT
  private static OSM: string = 'osm';
  private static BOUNDS: string = 'bounds';
  private static HOME: string = 'home';
  private static NODE: string = 'node';
  private static WAY: string = 'way';
  private static WAYNODE: string = 'nd';
  private static TAG: string = 'tag';
  private static ID: string = 'id';
  private static WAYNODEID: string = 'ref';
  private static POI: string = 'poi';
  private static POI_LABEL: string = 'label';

  private bounds: BoundingBox;
  private home: LatLonCoordinate;
  private ways: Map<string, Array<StreetSegment>>;
  private nodes: Map<string, LatLonCoordinate>;
  private pois: Map<string, POI>;

  constructor() {
    this.bounds = null;
    this.home = null;
    this.ways = new Map<string, Array<StreetSegment>>();
    this.nodes = new Map<string, LatLonCoordinate>();
    this.pois = new Map<string, POI>();
  }

  public parseOSM(osm: string): Promise<TrainingArea> {
    let promise = new Promise<TrainingArea>((resolve, reject) => {
      parseString(osm, (err, result) => {
        if (err) {
          reject(err);
        } else {
          try {
            this.parseRoot(result);
            resolve(this.consume());
          } catch (e) {
            reject(e);
          }
        }
      });
    });
    return promise;
  }

  private consume(): TrainingArea {
    let ta = new TrainingArea(this.home, this.bounds, this.pois);
    this.ways.forEach((segments, name, map) => {
      let street = new Street(name);
      for (let segment of segments) {
        street.add(segment);
      }
      ta.addStreet(street);
    });
    return ta;

  }

  private getAttribute(name: string, node: any): string {
    if (node['$'] && node['$'][name]) {
      return node['$'][name];
    }
    throw new Error(`Expected attribute ${name} in ${JSON.stringify(node)}`);
  }

  private getChildren(name: string, node: any): Array<any> {
    let children = node[name];
    if (children) {
      if (children instanceof Array) {
        return children;
      }
      return [children];
    }
    throw new Error(`Expected child node(s) ${name} in ${JSON.stringify(node)}`);
  }

  private getChild(name: string, node: any): any {
    let child = this.getChildren(name, node);
    if (!(child instanceof Array)) {
      return child;
    }
    if (child.length == 1) {
      return child[0];
    } else {
      throw new Error(`Expected only 1 child ${name} in ${JSON.stringify(node)}`);
    }
  }

  private parseCoord(lat: string, lon: string): LatLonCoordinate {
    let la = Number(lat);
    let lo = Number(lon);
    if (la !== NaN && lo != NaN) {
      return new LatLonCoordinate(la, lo);
    } else {
      throw new Error(`Could not parse ${lat}, ${lon} to numbers for coordinates`);
    }
  }

  private getCoord(node: any): LatLonCoordinate {
    return this.parseCoord(
      this.getAttribute('lat', node),
      this.getAttribute('lon', node)
    );
  }

  private getTag(key: string, node: any): string {
    for (let tag of this.getChildren(FortesoOSMParser.TAG, node)) {
      if (this.getAttribute('k', tag) === key) {
        return this.getAttribute('v', tag);
      }
    }
    throw new Error(`Node ${JSON.stringify(node)} doesn't have a tag ${key}`);
  }

  private parseRoot(root): void {
    let osm = this.getChild(FortesoOSMParser.OSM, root);
    let bounds = this.getChild(FortesoOSMParser.BOUNDS, osm);
    this.bounds = new BoundingBox(
      this.parseCoord(this.getAttribute('minlat', bounds), this.getAttribute('minlon', bounds)),
      this.parseCoord(this.getAttribute('maxlat', bounds), this.getAttribute('maxlon', bounds)));
    let home = this.getChild(FortesoOSMParser.HOME, osm);
    this.home = this.getCoord(home);

    let nodes = this.getChildren(FortesoOSMParser.NODE, osm);
    for (let node of nodes) {
      let id = this.getAttribute(FortesoOSMParser.ID, node);
      let coord = this.getCoord(node);
      this.nodes.set(id, coord);
    }

    let ways = this.getChildren(FortesoOSMParser.WAY, osm);
    for (let way of ways) {
      let waynodes = this.getChildren(FortesoOSMParser.WAYNODE, way);
      let segment = new StreetSegment();
      for (let waynode of waynodes) {
        let id = this.getAttribute(FortesoOSMParser.WAYNODEID, waynode);
        if (!this.nodes.has(id)) {
          throw new Error(`Waynode ${id} is unknown in ${JSON.stringify(way)}`);
        }
        segment.add(this.nodes.get(id));
      }

      let name = this.getTag('name', way);
      if (this.ways.has(name)) {
        this.ways.get(name).push(segment);
      } else {
        this.ways.set(name, [segment]);
      }
    }

    let pois = this.getChildren(FortesoOSMParser.POI, osm);
    for(let poi of pois) {
      let coord = this.getCoord(poi);
      let label = this.getAttribute(FortesoOSMParser.POI_LABEL, poi);
      let poiModel = new POI(coord, label);
      this.pois.set(poiModel.label, poiModel);
    }
  }
}
