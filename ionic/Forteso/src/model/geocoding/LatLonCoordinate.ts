import LatLngLiteral from 'leaflet-map';
import * as L from 'leaflet';

export default class LatLonCoordinate {
  constructor(public readonly lat: number, public readonly lon: number) {

  }

  public distance(other: LatLonCoordinate): number {
    return L.latLng(this.toLeafletLatLng()).distanceTo(L.latLng(other.toLeafletLatLng()));
  }

  public toLeafletLatLng(): LatLngLiteral {
    return [this.lat, this.lon];
  }
}
