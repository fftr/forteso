import LatLonCoordinate from './LatLonCoordinate';


export class StreetSegment {
  private nodes: Array<LatLonCoordinate>;

  constructor() {
    this.nodes = [];
  }

  public add(latlon: LatLonCoordinate): void {
    this.nodes.push(latlon);
  }

  public findClosestPoint(coord: LatLonCoordinate): ClosestPoint {
    let s = this.nodes.map(latlon => new ClosestPoint(latlon, coord.distance(latlon)));
    return s.reduce((acc, e) => (acc.distance < e.distance) ? acc : e, ClosestPoint.INDEFINITE);
  }

  public getNodes(): Array<LatLonCoordinate> {
    return this.nodes;
  }
}

export class ClosestPoint {
  public static INDEFINITE: ClosestPoint = new ClosestPoint(null, Number.POSITIVE_INFINITY);
  constructor(public readonly coord: LatLonCoordinate, public readonly distance: number) { }
}



export class Street {
  private segments: Array<StreetSegment>;

  constructor(private name: string) {
    this.segments = [];
  }

  public add(segment: StreetSegment): void {
    this.segments.push(segment);
  }

  public getName(): string {
    return this.name;
  }

  public findClosestPoint(coord: LatLonCoordinate): ClosestPoint {
    return this.segments.map(segment => segment.findClosestPoint(coord))
      .reduce((acc, e) => (acc.distance < e.distance) ? acc : e, ClosestPoint.INDEFINITE);
  }

  public getSegments(): Array<StreetSegment> {
    return this.segments;
  }
}
