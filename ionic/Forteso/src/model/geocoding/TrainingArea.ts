import LatLonCoordinate from './LatLonCoordinate';
import { Street } from './Street';
import BoundingBox from './BoundingBox';
import { POI } from './poi';

export class TrainingArea {

  streets: Map<string, Street>;

  constructor(
    public readonly home: LatLonCoordinate,
    public readonly bounds: BoundingBox,
    public readonly pois: Map<string, POI>) {
    this.streets = new Map<string, Street>();
  }

  public addStreet(street: Street): void {
    if (this.streets.has(street.getName())) {
      throw new Error(`Street ${street.getName()} has already been added`);
    }
    this.streets.set(street.getName(), street);
  }

  public getRandomStreet(): Street {
    let rndIndex = Math.floor(Math.random() * this.streets.size);
    return Array.from(this.streets.values())[rndIndex];
  }

  public getRandomPOI(): POI {
    let rndIndex = Math.floor(Math.random() * this.pois.size);
    return Array.from(this.pois.values())[rndIndex];
  }

  public getRandomElement(useStreets: boolean, usePOIs: boolean): Street | POI {
    if(useStreets && !usePOIs) {
      return this.getRandomStreet();
    }else if(!useStreets && usePOIs) {
      return this.getRandomPOI();
    }else if(useStreets && usePOIs) {
      let rndIndex = Math.floor(Math.random() * (this.streets.size + this.pois.size))
      if (rndIndex < this.streets.size) {
        return Array.from(this.streets.values())[rndIndex];
      } else {
        return Array.from(this.pois.values())[rndIndex - this.streets.size];
      }
    }else {
      throw new Error("useStreet and usePOI is false");
    }

  }


}
