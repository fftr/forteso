import LatLonCoordinate from './LatLonCoordinate';

export class POI {
  constructor(readonly coordinate: LatLonCoordinate, readonly label: string) {
    
  }
}
