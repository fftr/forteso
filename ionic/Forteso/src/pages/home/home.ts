import { Component, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { NavController, AlertController } from 'ionic-angular';
import { MapDataProvider } from '../../providers/map-data-provider';
import { MapComponent } from '../../components/map-component/map-component';
import { StateProvider } from '../../providers/state-provider';
import {ErrorHandler} from '../../providers/error-handler';

import { Street } from '../../model/geocoding/Street';
import { POI } from '../../model/geocoding/poi';
import LatLonCoordinate from '../../model/geocoding/LatLonCoordinate';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private static locationDataURI: string = 'assets/locationdata/location.xml';
  private static readonly STORAGE_IO_WARNING = 'Ein Wert konnte nicht gelesen oder gespeichert werden.\nDein Fortschritt kann wahrscheinlich nicht gespeichert werden';

  addressToFind: string;
  distance: string;
  nrCorrect: number;
  nrIncorrect: number;
  @ViewChild('mapcomponent') mapComponent: MapComponent;

  private elementToFind: Street | POI;
  private revealed: boolean;

  constructor(public navCtrl: NavController, private mapData: MapDataProvider,
    private http: Http, private state: StateProvider, private alertCtrl: AlertController,
  private error: ErrorHandler) {
    this.addressToFind = '';
    this.distance = '';
    //bind
    this.state.nrCorrect.subscribe(val => this.nrCorrect = val, err => this.error.reportWarning(err, HomePage.STORAGE_IO_WARNING));
    this.state.nrIncorrect.subscribe(val => this.nrIncorrect = val, err => this.error.reportWarning(err, HomePage.STORAGE_IO_WARNING));
  }

  ngOnInit() {
    this.http.get(HomePage.locationDataURI).map((responseData) => responseData.text()).subscribe(s => {
      this.mapData.load(s, () => {
        this.mapComponent.initMap(this.mapData.getBounds());
        this.mapComponent.setClickHandler(ll => this.selectLocation(ll));
        this.mapComponent.placeHomeMarker(this.mapData.getHome());

        //this is a dirty hack: the mapcomponent does not register left clicks for some reason, any interaction with any other component will fix this
        // => the user has to click the button and therefore make the mapcomponent clickable
        //this.nextAddressBtnClicked();
        this.mapComponent.setLabelVisible(true);
      }, (err) => {
        this.error.reportError(err, 'Beim Laden der Karten-Geodaten ist ein Fehler aufgetreten');
      });
    });
  }

  public nextAddressBtnClicked(): void {
    this.mapComponent.removeGreenMarker();
    this.mapComponent.removeBlueMarker();
    this.mapComponent.removeStreetPolyline();
    this.mapComponent.removeClosestPointPolyline();
    this.distance = '';
    this.elementToFind = this.mapData.getRandomElement();
    let label = (this.elementToFind instanceof Street) ? this.elementToFind.getName() : this.elementToFind.label;
    this.addressToFind = `Finde ${label}`;
    this.mapComponent.setLabelVisible(false);
    this.revealed = false;
  }

  private selectLocation(coord: LatLonCoordinate): void {
    if (!this.revealed) {
      this.revealed = true;
      this.mapComponent.placeGreenMarker(coord);
      let distance: number = 0;
      let connectorCoord: LatLonCoordinate = null;
      if (this.elementToFind instanceof Street) {
        //Street
        let res = this.elementToFind.findClosestPoint(coord);
        distance = res.distance;
        connectorCoord = res.coord;

        this.mapComponent.placeStreetPolyline(this.elementToFind.getSegments().map(seg => seg.getNodes()), 'blue', 10, 1);
      } else {
        //POI
        this.mapComponent.placeBlueMarker(this.elementToFind.coordinate);
        distance = coord.distance(this.elementToFind.coordinate);
        connectorCoord = this.elementToFind.coordinate;
      }

      this.mapComponent.setLabelVisible(true);
      this.distance = `Distanz: ${Math.floor(distance)}m`;
      this.mapComponent.placeClosestPointPolyline(coord, connectorCoord, 'black', 2, 1);
      if (distance <= this.state.getThreshold()) {
        this.state.incrCorrect();
      } else {
        this.state.incrIncorrect();
      }
    }
  }

  showHelp() {
    let message = '';
    if(this.addressToFind === '') {
      message = 'Benutze den "Nächste Aufgabe" Knopf rechts unten um eine neue Aufgabe zu erhalten.';
    }else if(this.addressToFind !== '' && this.distance === '') {
      message = 'Suche nun den Ort, der links unten angezeigt wird. Klicke auf die Karte wo du ihn vermutest';
    }else if(this.distance !== '') {
      message = 'Nun siehst du das richtige Ergebnis. Drücke wieder auf den "Nächste Aufgabe" Knopf.';
    }else{
      message = 'Besuche unsere Seite unter https://gitlab.com/fftr/forteso';
    }
    this.alertCtrl.create({
      title: 'Hilfe',
      message: message,
      buttons: [
        'Ok'
      ]
    }).present();
  }

}
