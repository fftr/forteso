import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { StateProvider } from '../../providers/state-provider';

/**
 * Generated class for the Settings page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  nrCorrect: number = 10;
  nrIncorrect: number = 1;
  threshold: number;

  useStreets: boolean = true;
  usePOIs: boolean = true;

  url: string;
  urlNoLabel: string;

  constructor(private state: StateProvider, private alertCtrl: AlertController) {
    this.state.nrCorrect.subscribe(v => this.nrCorrect = v);
    this.state.nrIncorrect.subscribe(v => this.nrIncorrect = v);
    this.threshold = this.state.getThreshold();
    this.state.tileServerURL.subscribe(v => this.url = v);
    this.state.tileServerNoLabelURL.subscribe(v => this.urlNoLabel = v);
    this.useStreets = this.state.getUseStreets();
    this.usePOIs = this.state.getUsePOIs();
  }

  ionViewDidLoad() {

  }

  onChangeThreshold() {
    this.save(() => this.state.setThreshold(this.threshold));
  }

  resetProgress() {
    this.state.resetProgress();
  }

  resetURLs() {
    this.state.resetTileServerURLs();
  }

  saveURL() {
    this.save(() => this.state.setTileServerURL(this.url));
  }

  saveURLNoLabel() {
    this.save(() => this.state.setTileServerNoLabelURL(this.urlNoLabel));
  }

  useStreetsChanged() {
    this.save(() => this.state.useStreets(this.useStreets));
    if (!this.useStreets && !this.usePOIs) {
      this.usePOIs = true;
      this.save(() => this.state.usePOIs(true));
      this.showNoDataAlert();
    }
  }

  usePOIsChanged() {
    this.save(() => this.state.usePOIs(this.usePOIs));
    if (!this.useStreets && !this.usePOIs) {
      this.useStreets = true;
      this.save(() => this.state.useStreets(true));
      this.showNoDataAlert();
    }
  }

  private save(fun: () => Promise<void>) {
    fun().catch(err => window.alert(err));
  }

  private showNoDataAlert() {
    this.alertCtrl.create({
      title: 'Ungültige Auswahl',
      subTitle: 'Es muss mindestens eine Datenquelle ausgewählt sein (Straßen bzw. wichtige Punkte)',
      buttons: [
        'Ok'
      ]
    }).present();

  }
}
