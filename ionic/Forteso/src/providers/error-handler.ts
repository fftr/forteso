import { Injectable } from '@angular/core';
import {AlertController} from 'ionic-angular';

/*
  Generated class for the ErrorHandler provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ErrorHandler {

  constructor(private alertCtrl: AlertController) {

  }

  reportError(err: any, msg? : string) {
    let message = `Ein Fehler ist aufgetreten.\nDetails: ${msg}\nSchau mal in der Konsole nach und melde den Fehler!`;
    let alert = this.alertCtrl.create({
      title: 'Oje, ein Fehler',
      message: message,
      buttons: [
        'Ok'
      ]
    });
    alert.present();
    console.log(err);
  }

  reportWarning(err? : any, msg?: string) {
    let alert = this.alertCtrl.create({
      title: 'Warnung',
      message: msg,
      buttons: [
        'Ok'
      ]
    });
    alert.present();
    console.log('Warnung '+msg);
    console.log(err);
  }

}
