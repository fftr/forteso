import { Injectable } from '@angular/core';

import FortesoOSMParser from '../model/geocoding/FortesoOSMParser';
import { TrainingArea } from '../model/geocoding/TrainingArea';
import { Street } from '../model/geocoding/Street';
import { POI } from '../model/geocoding/poi';
import BoundingBox from '../model/geocoding/BoundingBox';
import LatLonCoordinate from '../model/geocoding/LatLonCoordinate';

import {StateProvider} from './state-provider';
/*
  Generated class for the MapDataService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MapDataProvider {

  private trainingArea: TrainingArea;

  constructor(private state: StateProvider) {

  }

  public load(content: string, success: () => void, fail: (error) => void) {
    let parser = new FortesoOSMParser();
    parser.parseOSM(content)
      .then((result) => {
        this.trainingArea = result;
        success();
      })
      .catch((error) => {
        fail(error);
      });
  }

  public listStreets(): string {
    var s = "";
    this.trainingArea.streets.forEach((street, name, map) => {
      s += name + "\n";
    });
    return s;
  }

  public getRandomElement(): Street | POI {
    return this.trainingArea.getRandomElement(this.state.getUseStreets(), this.state.getUsePOIs());
  }

  public getBounds(): BoundingBox {
    return this.trainingArea.bounds;
  }

  public getHome(): LatLonCoordinate {
    return this.trainingArea.home;
  }

}
