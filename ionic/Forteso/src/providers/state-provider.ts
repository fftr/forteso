import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { ErrorHandler } from './error-handler';

/*
  Generated class for the MapDataService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StateProvider {

  private static readonly tileServerURLDefault: string = 'http://b.tiles.wmflabs.org/osm/{z}/{x}/{y}.png';
  private static readonly tileServerURLNoLabelDefault: string = 'http://b.tiles.wmflabs.org/osm-no-labels/{z}/{x}/{y}.png';
  private static readonly correctThresholdDefault: number = 30;

  private static readonly NR_CORRECT = 'nrcorrect';
  private static readonly NR_INCORRECT = 'nrincorrect';
  private static readonly THRESHOLD = 'threshold';
  private static readonly TILE_URL = 'tileurl';
  private static readonly TILE_NO_LABEL_URL = 'tilenolabelurl';
  private static readonly USE_STREETS = 'usestreets';
  private static readonly USE_POIS = 'usepois';


  private _nrCorrect: BehaviorSubject<number> = new BehaviorSubject(0);
  private _nrIncorrect: BehaviorSubject<number> = new BehaviorSubject(0);
  private correctThreshold: number = StateProvider.correctThresholdDefault;

  private _tileServerURL: BehaviorSubject<string> = new BehaviorSubject(StateProvider.tileServerURLDefault);
  private _tileServerNoLabelURL: BehaviorSubject<string> = new BehaviorSubject(StateProvider.tileServerURLNoLabelDefault);

  private _useStreets: boolean = true;
  private _usePOIs: boolean = true;

  constructor(private storage: Storage, private error: ErrorHandler) {
    this.storage.ready().then(forage => {
      console.log('Storage ready');
      this.storage.get(StateProvider.NR_CORRECT)
        .then(v => {
          if (v) {
            this._nrCorrect.next(v);
          }
        }).catch(err => this._nrCorrect.error(err));
      this.storage.get(StateProvider.NR_INCORRECT)
        .then(v => {
          if (v) {
            this._nrIncorrect.next(v);
          }
        }).catch(err => this._nrIncorrect.error(err));
      this.storage.get(StateProvider.THRESHOLD)
        .then(v => {
          if (v) {
            this.correctThreshold = v;
          }
        })
        .catch(err => this.error.reportError(err, 'Beim Laden des Threshold-Werts ist ein Fehler aufgetreten'));
      this.storage.get(StateProvider.TILE_URL)
        .then(url => {
          if (url) {
            this._tileServerURL.next(url);
          }
        }).catch(err => this._tileServerURL.error(err));
      this.storage.get(StateProvider.TILE_NO_LABEL_URL)
        .then(url => {
          if (url) {
            this._tileServerNoLabelURL.next(url);
          }
        }).catch(err => this._tileServerNoLabelURL.error(err));
      this.storage.get(StateProvider.USE_STREETS)
        .then(use => {
          if (use !== undefined && use !== null) {
            this._useStreets = use;
          }
          if (!this._usePOIs && !this._useStreets) {
            //illegal state -> reset
            this._usePOIs = true;
            this._useStreets = true;
          }
        }).catch(err => this.error.reportError(err, 'Beim Laden des "Nutze Straßen"-Werts ist ein Fehler aufgetreten'));
      this.storage.get(StateProvider.USE_POIS)
        .then(use => {
          if (use !== undefined && use !== null) {
            this._usePOIs = use;
          }
          if (!this._usePOIs && !this._useStreets) {
            //illegal state -> reset
            this._usePOIs = true;
            this._useStreets = true;
          }
        }).catch(err => this.error.reportError(err, 'Beim Laden des "Nutze wichtige Punkte"-Werts ist ein Fehler aufgetreten'));
    }).catch(/*storage error */ err => this.error.reportWarning(err, 'Dein Browser scheint keinen lokalen Speicher zu unterstützen oder Forteso darf nicht darauf zugreifen.\nDein Fortschritt wird nicht gespeichert.'));
  }

  public get nrCorrect(): Observable<number> {
    return this._nrCorrect;
  }

  public get nrIncorrect(): Observable<number> {
    return this._nrIncorrect;
  }

  public get tileServerURL(): Observable<string> {
    return this._tileServerURL;
  }

  public get tileServerNoLabelURL(): Observable<string> {
    return this._tileServerNoLabelURL;
  }

  public getThreshold(): number {
    return this.correctThreshold;
  }

  public setThreshold(t: number): Promise<void> {
    this.correctThreshold = t;
    return this.storage.set(StateProvider.THRESHOLD, this.correctThreshold);
  }

  private compute(prop: BehaviorSubject<number>, save: string, fun: (val: number) => number) {
    let val = prop.getValue();
    this.storage.set(save, fun(val))
      .then(v => prop.next(v))
      .catch(err => prop.error(err));
  }

  public incrCorrect(): void {
    this.compute(this._nrCorrect, StateProvider.NR_CORRECT, v => v + 1);
  }

  public incrIncorrect(): void {
    this.compute(this._nrIncorrect, StateProvider.NR_INCORRECT, v => v + 1);
  }

  public resetProgress() {
    this.compute(this._nrCorrect, StateProvider.NR_CORRECT, v => 0);
    this.compute(this._nrIncorrect, StateProvider.NR_INCORRECT, v => 0);
  }

  public setTileServerURL(url: string): Promise<void> {
    return this.storage.set(StateProvider.TILE_URL, url)
      .then(_ => this._tileServerURL.next(url));
  }

  public setTileServerNoLabelURL(url: string): Promise<void> {
    return this.storage.set(StateProvider.TILE_NO_LABEL_URL, url)
      .then(_ => this._tileServerNoLabelURL.next(url));
  }

  public resetTileServerURLs() {
    this.setTileServerURL(StateProvider.tileServerURLDefault);
    this.setTileServerNoLabelURL(StateProvider.tileServerURLNoLabelDefault);
  }

  public useStreets(use: boolean): Promise<void> {
    this._useStreets = use;
    return this.storage.set(StateProvider.USE_STREETS, use);
  }

  public usePOIs(use: boolean): Promise<void> {
    this._usePOIs = use;
    return this.storage.set(StateProvider.USE_POIS, use);
  }

  public getUseStreets(): boolean {
    return this._useStreets;
  }

  public getUsePOIs(): boolean {
    return this._usePOIs;
  }

}
